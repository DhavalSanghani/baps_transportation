import templeLogoImage from './logo_login_ic.png';
import coverPlaceholder from './coverPlaceholder.png';
import userPlaceholder from './user_placeholder_ic.png';
import messageImage from './message_images.png';
import iCardImage from './iCard.png';

export {
  templeLogoImage,
  coverPlaceholder,
  userPlaceholder,
  messageImage,
  iCardImage,
};
