import eyeOffIcon from './eye_off_ic.png';
import eyeOnIcon from './eye_on_ic.png';
import homeIcon from './home_ic.png';
import membersIcon from './members_ic.png';
import profileIcon from './profile_ic.png';
import attendanceIcon from './attendance_ic.png';
import reportsIcon from './reports_ic.png';
import cameraIcon from './camera_ic.png';
import deleteIcon from './delete.png';
import notificationIcon from './notification_ic.png';
import leftArrowIcon from './left_arrow_ic.png';
import closeIcon from './close_ic.png';
import rightArrowIcon from './right_arrow_ic.png';
import checkedGreenIcon from './checked_ic.png';
import achievementIcon from './achievements_Ic.png';

import galleryIcon from './gallery_ic.png';
import smsIcon from './sms_ic.png';
import whatsappIcon from './whatsApp_ic.png';
import callIcon from './call_ic.png';
import linkIcon from './link_ic.png';

import linkRedIcon from './red_link_ic.png';

import checkIcon from './check_ic.png';
import bottomDropdownArrowIcon from './bottom_dropdown_arrow_ic.png';
import upDropdownArrowIcon from './up_dropdown_arrow_ic.png';

import searchIcon from './search_ic.png';
import calendarIcon from './calendar_ic.png';
import removeIcon from './remove_ic.png';
import sortingIcon from './sort_black_ic.png';
import filterIcon from './filter_list_black_ic.png';

import subusersIcon from './subusers_ic.png';
import addIcon from './add_ic.png';
import aTOzSortIcon from './a_to_z_sort_ic.png';
import zTOaSortIcon from './z_to_a_sort_ic.png';
import locationIcon from './pin_drop_black_24dp.png';
import groupIcon from './groups_black_24dp.png';
import idCardIcon from './assignment_ind_black_24dp.png';

import editCustomIcon from './edit_custom_ic.png';
import editPencilIcon from './edit_pencil_ic.png';
import trashIcon from './trash_ic.png';

import watchIcon from './watch_ic.png';
import threeDotIcon from './three_dot_ic.png';
import qrCodeIcon from './qr_code_black_ic.png';

import qrCodeScannerIcon from './qr_code_scanner.png';
import infoIcon from './info.png';
import excelExportIcon from './excel_export_ic.png';
import cameraBGIcon from './cameraBG.png';
import downloadIcon from './download.png';
import dottedOrLineIcon from './dotted_or_line_ic.png';
import veryBadEmojiIcon from './very_bad.png';
import badEmojiIcon from './bad.png';
import normalEmojiIcon from './normal_emoji.png';
import goodEmojiIcon from './good_emoji.png';
import veryGoodEmojiIcon from './very_good_emoji.png';
import assessmentIcon from './assessment_ic.png';

import grayVeryBadEmojiIcon from './gray_very_bad.png';
import grayBadEmojiIcon from './gray_bad.png';
import grayNormalEmojiIcon from './gray_normal.png';
import grayGoodEmojiIcon from './gray_good.png';
import grayVeryGoodEmojiIcon from './amazing.png';

import grayDefaultIcon from './gray_normal_smily_ic.png';

import recordNotFoundIcon from './recordnotfound_ic.png';
import searchPlaceholderIcon from './search_placeholder.png';
import whiteCheckIcon from './white_check_ic.png';
import scrollToTopIcon from './scroll_to_top_ic.png';
import splashLogo from './logo.png';
import noInterNetIcon from './noInterNetIcon.png';
import maintananceIcon from './maintananceIcon.png';
import sabhaSummaryIcon from './sabhaSummary_ic.png';

import upArrowIcon from './up-arrow.png';
import downArrowIcon from './down-arrow.png';

export {
  linkRedIcon,
  downArrowIcon,
  upArrowIcon,
  recordNotFoundIcon,
  searchPlaceholderIcon,
  eyeOffIcon,
  eyeOnIcon,
  homeIcon,
  membersIcon,
  profileIcon,
  attendanceIcon,
  reportsIcon,
  cameraIcon,
  deleteIcon,
  notificationIcon,
  leftArrowIcon,
  closeIcon,
  rightArrowIcon,
  checkedGreenIcon,
  achievementIcon,
  galleryIcon,
  smsIcon,
  whatsappIcon,
  callIcon,
  linkIcon,
  checkIcon,
  bottomDropdownArrowIcon,
  upDropdownArrowIcon,
  searchIcon,
  calendarIcon,
  removeIcon,
  filterIcon,
  sortingIcon,
  subusersIcon,
  addIcon,
  aTOzSortIcon,
  zTOaSortIcon,
  locationIcon,
  groupIcon,
  idCardIcon,
  editCustomIcon,
  editPencilIcon,
  trashIcon,
  watchIcon,
  threeDotIcon,
  qrCodeIcon,
  qrCodeScannerIcon,
  infoIcon,
  excelExportIcon,
  cameraBGIcon,
  downloadIcon,
  dottedOrLineIcon,
  veryBadEmojiIcon,
  badEmojiIcon,
  normalEmojiIcon,
  goodEmojiIcon,
  veryGoodEmojiIcon,
  assessmentIcon,
  whiteCheckIcon,
  scrollToTopIcon,
  splashLogo,
  noInterNetIcon,
  maintananceIcon,
  sabhaSummaryIcon,
  grayBadEmojiIcon,
  grayDefaultIcon,
  grayGoodEmojiIcon,
  grayNormalEmojiIcon,
  grayVeryBadEmojiIcon,
  grayVeryGoodEmojiIcon,
};
