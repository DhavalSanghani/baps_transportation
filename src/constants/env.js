const isLive = true;
const isLogDisplay = false;
let api_base_uri = 'http://192.168.1.10/BAPS_Yuva/api/v1';
if (isLive) {
  api_base_uri =
    'https://www.yuvasurat.solwininfotech.com/appservice/Seva/api/v1';
}
const mediaOption = {
  camera: 'camera',
  gallery: 'gallery',
};
export {api_base_uri, mediaOption, isLogDisplay, isLive};
