export const fonts = {
  SFProTextBold: 'SFProText-Bold',
  SFProTextRegular: 'SFProText-Regular',
  SFProTextMedium: 'SFProText-Medium',
  SFProTextSemiBold: 'SFProText-SemiBold',
};
