import colors from './colors';
const commonStyles = {
  profileAvatarStyle: {
    contentContainerStyle: {
      borderWidth: 4,
      borderColor: colors.grayDisable,
      padding: 5,
    },
    editIconStyle: {
      tintColor: colors.black,
      width: 24,
      height: 24,
    },
    editIconContainerStyle: {
      width: 40,
      height: 40,
    },
  },
  bottomInput: {
    // inputContainerStyle: {
    //   paddingHorizontal: 0,
    //   borderWidth: 0,
    //   borderBottomWidth: 1,
    //   borderRadius: 0,
    // },
    // titleStyle: {left: 0},
  },
  dropdownStyle: {
    contentContainerStyle: {
      paddingHorizontal: 0,
      borderWidth: 0,
      borderBottomWidth: 0.6,
      borderRadius: 0,
    },
    dropdownBoxtitleContainerStyle: {left: 0, paddingHorizontal: 0},
    selectTextStyle: {marginLeft: 0},
  },
  dropShadow: {
    shadowColor: colors.darkBlack,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.05,
    shadowRadius: 4,
  },
  topTabNavigatorContainerStyle: {
    shadowColor: colors.darkBlack,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.05,
    shadowRadius: 4,
  },
  shadowContainerStyle: {
    shadowColor: colors.darkBlack,
    shadowOffset: {
      width: 0,
      height: -5,
    },
    shadowOpacity: 0.05,
    shadowRadius: 4,
  },
};

export default commonStyles;
