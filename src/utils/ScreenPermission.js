/*
- Only below role person can access Xetra and Mandal sorting 
- uses location 
* Members -> All Member -> index.js
*/
const __AllMemberSortingAccess = role => {
  return [1, 2].includes(role);
};

/*
- Only below role person can access Member tab 
- uses location 
* Members -> index.js
*/
const __MemberAllAccess = role => {
  return [1, 2, 3, 4, 5, 6, 7, 99].includes(role);
};
const __FilterSatsangAccess = role => {
  return [1, 2].includes(role);
};
const __CustomListAccess = role => {
  return [1, 2, 3, 4, 5, 6, 7, 99].includes(role);
};
const __SamparkKaryakarListAccess = role => {
  return [1, 2, 3, 4, 7].includes(role);
};
const __MandalSevakListAccess = role => {
  return [1, 2, 3, 4, 5, 7].includes(role);
};
const __EditProfileAccess = role => {
  return [3, 4].includes(role);
};
const __EditProfileExamAccess = role => {
  return [3, 4, 5].includes(role);
};
/*
- Only below role person can access Self profile Edit 
- uses location 
* Profile -> ProfilePages -> EditProfiles -> index.js
* Profile -> ProfilePages -> all JS File except setting
*/
const __EditSatsangExamInfoAccess = role => {
  return [1, 2, 3, 4, 5].includes(role);
};
const __EditSatsangYTKInfoAccess = role => {
  return [1, 2, 3, 4].includes(role);
};
const __EditSatsangYSSInfoAccess = role => {
  return [3, 4, 7].includes(role);
};
const __EditMandalSevakInfoAccess = role => {
  return [1, 2, 3, 4].includes(role);
};

/*
- Only below role person can access reminder and message
- uses location 
* Notification -> index.js
*/
const __reminderAccess = role => {
  return [3, 4].includes(role);
};
const __MessageAccess = role => {
  return [1, 2, 3, 4, 99].includes(role);
};
/*
Send Message function permission 
- uses location 
* Notification -> Message -> createMessage.js
*/
const __sendToRoleTypeAccess = role => {
  return [1, 2, 3, 4, 99].includes(role);
};
const __sendToMandalAccess = role => {
  return [2].includes(role);
};
const __sendToWingAccess = role => {
  return [1].includes(role);
};
const __sendToXetraAccess = role => {
  return [1].includes(role);
};

/*
- Only below role person can access Bottom tab 
- uses location 
* Navigation -> UserStack.js
*/
const __MemberTabAccess = role => {
  return [1, 2, 3, 4, 5, 6, 7, 99].includes(role);
};
const __ReportTabAccess = role => {
  return [1, 2, 3, 4, 5, 6, 7, 8].includes(role);
};
const __AssessmentBottomTabAccess = role => {
  return [1, 8].includes(role);
};
/*
- Only below role person can access Mandal Sevak Filter 
- uses location 
* Members -> MandalSevak -> MandalSevaSamparkKaryakar.js
*/
const __MandalSevakSevaAccess = role => {
  return [1, 2].includes(role);
};

/*
- Only below role person can access Self assesment 
- uses location 
* Profile -> Profile.js
*/
const __SelfAssesmentTabAccess = role => {
  return [1, 2, 3, 4, 5, 6, 7].includes(role);
};
/*
- Only below role person can access Self assesment 
- uses location 
* Profile -> Profile.js
*/
const __SelfAssesmentFilterAccess = role => {
  return [1].includes(role);
};

/*
- Only below role person can access Sabha Summary 
- uses location 
* Attendance -> index.js
*/
const __SabhaSummaryAccess = role => {
  return [1, 2, 3, 4, 99].includes(role);
};

/*
- Only below role person can access create Sabha
- uses location 
* Attendance -> index.js
*/
const __SabhaCreateAccess = role => {
  return [3, 4].includes(role);
};

/*
- Only below role person can access Sabha Summary filter
- uses location 
* Attendance -> SabhaSummary -> index.js
*/
const __SabhaSummaryFilterAccess = role => {
  return [1].includes(role);
};

/*
- Only below role person can access Attendance filter
- uses location 
* Report -> ReportAttendance -> index.js
* Report -> FilterReportModal.js
*/
const __ReportAttendanceFilterAccess = role => {
  return [1].includes(role);
};

/*
- Only below role person can access Self assesment tab in report
- uses location 
* Report -> SelfAssessment -> index.js
*/
const __ReportSelfAssesmentTabAccess = role => {
  return [1, 2, 3, 4].includes(role);
};

/*
- Only below role person can access satsang  shikshan pariksha
*/
const __SatsangShikshanParikshaAccess = role => {
  return [1, 2, 3, 4, 5].includes(role);
};

/*
- Only below role person can access satsang ytk
*/

const __SatsangYtk = role => {
  return [1, 2, 3, 4].includes(role);
};

/*
- Only below role person can access satsang yss
*/

const __SatsangYss = role => {
  return [3, 4, 6].includes(role);
};

/*
- Only below role person can access of Qr code
*/

const __QrCodeScannerAccess = role => {
  return [3, 4].includes(role);
};

/*
- Only below role person can access of edit other self assessment
- use location
* Members -> AllMembers -> EditMemberProfiles
*/
const __editSelfAssessment = role => {
  return [1, 2, 7].includes(role);
};

export {
  __reminderAccess,
  __sendToMandalAccess,
  __sendToRoleTypeAccess,
  __sendToWingAccess,
  __sendToXetraAccess,
  __CustomListAccess,
  __MandalSevakListAccess,
  __MemberAllAccess,
  __SamparkKaryakarListAccess,
  __EditProfileAccess,
  __EditProfileExamAccess,
  __EditMandalSevakInfoAccess,
  __EditSatsangExamInfoAccess,
  __EditSatsangYSSInfoAccess,
  __EditSatsangYTKInfoAccess,
  __MessageAccess,
  __MemberTabAccess,
  __ReportTabAccess,
  __MandalSevakSevaAccess,
  __SabhaSummaryAccess,
  __SabhaSummaryFilterAccess,
  __SabhaCreateAccess,
  __FilterSatsangAccess,
  __AssessmentBottomTabAccess,
  __ReportAttendanceFilterAccess,
  __AllMemberSortingAccess,
  __SatsangShikshanParikshaAccess,
  __SatsangYtk,
  __SatsangYss,
  __QrCodeScannerAccess,
  __ReportSelfAssesmentTabAccess,
  __SelfAssesmentTabAccess,
  __SelfAssesmentFilterAccess,
  __editSelfAssessment,
};
