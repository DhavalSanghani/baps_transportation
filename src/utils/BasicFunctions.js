import moment from 'moment';
import {Linking} from 'react-native';
import {isLogDisplay} from '../constants/env';

const __filterProfile = data => {
  const userData = data.data;
  const {
    area_array,
    business_array,
    district_array,
    exam_array,
    grade_array,
    job_array,
    mandal_seva_array,
    satsang_array,
    kshetra_array,
    satsang_data,
    mandal_array,
    study_array,
    skill_array,
    wing_array,
    criteria_array,
    skill_computer_array,
    software_developer_skills_array,
  } = data;
  let fullAddress = __createAddress(userData);
  console.log(userData);
  let returnData = {};
  returnData = {
    unique_id: userData.unique_id,
    member_id: userData.member_id,
    gender: userData.gender === 'M' ? 'Male' : 'Female',
    dob: {
      original: userData.dob !== '0000-00-00' ? userData.dob : '',
      converted:
        userData.dob !== '0000-00-00'
          ? moment(userData.dob).format('DD MMM yyyy')
          : '',
    },
    fullName: userData.first_name + ' ' + userData.last_name,
    first_name: userData.first_name,
    middle_name: userData.middle_name,
    last_name: userData.last_name,
    blood_group: userData.blood_group,
    marital_status: userData.marital_status,
    education_name:
      userData.education === '' ||
      userData.education === null ||
      userData.education === 'null'
        ? ''
        : userData.education,
    education_detail:
      userData.education === '' ||
      userData.education === null ||
      userData.education === 'null'
        ? ''
        : study_array.filter(
            fItem => String(fItem.education_id) === String(userData.education),
          )[0].education_detail,
    occupation:
      userData.study === 1
        ? ''
        : userData.job_business === 1
        ? userData.job
        : userData.business,
    study: userData.study,
    job: userData.job,
    job_business: userData.job_business,
    business: userData.business,
    contact_no: userData.contact_no,
    kshetra_name: userData.kshetra_name,
    profile_pic: userData.profile_pic,
    mandal_id: userData.mandal_id,
    mandal_name: userData.mandal_name,
    role: parseInt(userData.role),
    roleName: userData.roleName || userData?.role_name,
    whatsapp_no: userData.whatsapp_no,
    resi_contact_no: userData.resi_contact_no,
    email: userData.email,
    address: fullAddress,
    block_no: userData.block_no,
    society: userData.society,
    landmark_1: userData.landmark_1,
    landmark_2: userData.landmark_2,
    area_id: userData.area,
    area_name: userData.area_name,
    pin_code: userData.pin_code,
    father_name: userData.middle_name,
    father_mobile: userData.father_mobile,
    fatherMainbusiness:
      userData.father_other || userData.father_business || userData.father_job,
    father_business: userData.father_business,
    father_job: userData.father_job,
    father_other: userData.father_other,
    district_id: userData.district,
    district_name: userData.district_name,
    taluka_id: userData.taluka,
    taluka_name: userData.taluka_name,
    village: userData.village,
    mandal_array: mandal_array,
    mandal_seva:
      userData.mandal_seva &&
      userData.mandal_seva !== '' &&
      userData.mandal_seva !== null
        ? userData.mandal_seva
        : [],
    basic_skills:
      userData.skills !== '' && userData.skills !== null && userData.skills
        ? userData.skills.filter(item => skill_array.includes(item))
        : [],
    computer_skills:
      userData.skills !== '' && userData.skills !== null && userData.skills
        ? userData.skills.filter(item => skill_computer_array.includes(item))
        : [],
    software_skills:
      userData.skills !== '' && userData.skills !== null && userData.skills
        ? userData.skills.filter(item =>
            software_developer_skills_array.includes(item),
          )
        : [],
    other_software_skills:
      userData.skills !== '' && userData.skills !== null && userData.skills
        ? userData.skills.filter(
            element =>
              !software_developer_skills_array.includes(element) &&
              !skill_computer_array.includes(element) &&
              !skill_array.includes(element),
          )
        : [],
    achievement:
      userData.achievement !== '' &&
      userData.achievement !== null &&
      userData.achievement &&
      userData.achievement !== 'false' &&
      String(userData.achievement) !== ''
        ? userData.achievement
        : [],
    last_exam: userData.last_exam,
    passing_year: userData.passing_year,
    ssp_exam_grade: userData.ssp_exam_grade,
    ssp_id: userData.ssp_id,
    next_ssp_exam: userData.next_ssp_exam,
    course: userData.is_yss === 1 ? true : false,
    is_yss: userData.is_yss,
    yss_year: userData.yss_year,
    is_ytk: userData.is_ytk,
    ytk_year: userData.ytk_year,
    courseYear: userData.yss_year,
    shibir: userData.is_ytk === 1 ? true : false,
    shibirYear: userData.ytk_year,
    kshetra_id: userData.kshetra_id,
    mandal_seva_array: mandal_seva_array,
    skill_array: skill_array,
    skill_computer_array: skill_computer_array,
    software_developer_skills_array: software_developer_skills_array,
    wing_array: wing_array,
    wing_name: userData.wing_name,
    exam_array: exam_array,
    grade_array: grade_array,
    kshetra_array: kshetra_array,
    criteria_array: criteria_array,
    extra_notes: userData.extra_notes,
    SKaryakar1: userData.SKaryakar1,
    SKaryakar2: userData.SKaryakar2,
    group_name: userData.group_name,
  };
  // console.log("userData.achievement", );
  let satsang = [];
  Object.keys(userData).map(item => {
    if (userData[item] == 1) {
      satsang.push(item);
    }
  });
  satsang = satsang_array.filter(item => satsang.includes(item.short_name));

  let tempSatsang = [];
  satsang.map(item => tempSatsang.push(item.name));
  let tempSatsangArray = [];
  satsang_array.map(item => tempSatsangArray.push(item.name));

  let tempAreaArray = [];
  area_array.map(item => tempAreaArray.push(item.area_name));
  let mandal_temp_array = [];
  mandal_array &&
    mandal_array.map(item =>
      mandal_temp_array.push({
        mandal_id: item.mandal_id,
        mandal_name: item.mandal_name,
        kshetra_id: item.kshetra_id,
      }),
    );
  returnData = {
    ...returnData,
    is_other_software_skills:
      returnData.other_software_skills.length >= 0 ? true : false,
    satsang: tempSatsang,
    satsang_id_array: satsang_array,
    satsang_array: tempSatsangArray,
    business_array: business_array.map(item => {
      return {...item, business_name: item.business_name.trim()};
    }),
    job_array: job_array.map(item => {
      return {...item, job_name: item.job_name.trim()};
    }),
    area_array: area_array,
    study_array: study_array,
    district_array: district_array,
    mandal_temp_array: mandal_temp_array,
  };
  return returnData;
};

const __createAddress = userData => {
  let fullAddress = '';
  userData = __addBlank(userData);
  fullAddress +=
    userData.block_no != '' && userData.block_no != null
      ? userData.block_no + ', '
      : '';
  fullAddress +=
    userData.society != null &&
    (userData?.society?.trim() != '' || userData.society != '')
      ? userData.society + ', '
      : '';
  fullAddress +=
    userData?.landmark_1 != '' && userData.landmark_1 != null
      ? userData.landmark_1 + ', '
      : '';
  fullAddress +=
    userData?.landmark_2 != '' && userData.landmark_2 != null
      ? userData.landmark_2 + ', '
      : '';
  fullAddress +=
    userData?.area_name != '' && userData.area_name != null
      ? userData.area_name + ' '
      : '';
  fullAddress +=
    userData?.pin_code != '' && userData.pin_code != null
      ? userData?.pin_code
      : '';
  fullAddress = fullAddress == '' ? '-' : fullAddress;
  return fullAddress;
};

const __selectDeselect = (value, data, matchKey) => {
  let checkData = data;

  if (
    checkData.filter(item =>
      matchKey ? item[matchKey] === value[matchKey] : item === value,
    ).length === 0
  ) {
    checkData.push(value);
  } else {
    checkData = checkData.filter(item =>
      matchKey ? item[matchKey] !== value[matchKey] : item !== value,
    );
  }

  return checkData;
};
const __getYears = () => {
  var currentYear = new Date().getFullYear() - 1;
  var years = [];
  let startYear = startYear || 2000;
  while (startYear <= currentYear) {
    years.push((startYear++).toString());
  }
  return years.reverse();
};

function __localSearch(dataArray, searchText, searchableDataArray) {
  if (/^[^a-zA-Z0-9]+$/.test(searchText)) return [];
  let text = searchText.replace(/[^\w\s]/gi, '');
  let regexp = new RegExp(text, 'gi');
  let tempData = [];

  if (
    searchableDataArray &&
    searchableDataArray.length !== 0 &&
    searchableDataArray !== null &&
    searchableDataArray !== undefined
  ) {
    searchableDataArray.forEach(searchableValue => {
      dataArray.forEach(dataValue => {
        if (String(dataValue[searchableValue]).match(regexp)) {
          const isAvailable = tempData.some(
            childValue => dataValue == childValue,
          );
          if (!isAvailable) {
            tempData.push(dataValue);
          }
        }
      });
    });
  } else {
    tempData = dataArray.filter(e =>
      Object.values(e)
        .join('')
        .toLowerCase()
        .match(regexp),
    );
  }

  return tempData;
}
function __arrangeSwipableData(data) {
  if (!data) return [];
  data = data.map((item, i) => {
    item.key = String(i);
    return item;
  });
  return data;
}
function __addDash(data, addAdditional) {
  let tempData = {};
  let checkArray = [
    '',
    'NaN',
    NaN,
    null,
    undefined,
    'undefined',
    'null',
    'Null',
  ];
  checkArray =
    addAdditional && addAdditional.length >= 1
      ? [...checkArray, ...addAdditional]
      : checkArray;
  Object.keys(data).map(item => {
    const check = checkArray.includes(data[item]);
    tempData = {
      ...tempData,
      [item]: check
        ? '-'
        : typeof data[item] == 'string'
        ? data[item].trim()
        : data[item],
    };
  });

  return tempData;
}

function __addBlank(data, addAdditional) {
  let tempData = {};
  let checkArray = [
    '',
    '-',
    'NaN',
    NaN,
    null,
    undefined,
    'undefined',
    'null',
    'Null',
  ];
  checkArray =
    addAdditional && addAdditional.length >= 1
      ? [...checkArray, ...addAdditional]
      : checkArray;
  Object.keys(data).map(item => {
    const check = checkArray.includes(data[item]);
    tempData = {
      ...tempData,
      [item]: check
        ? ''
        : typeof data[item] == 'string'
        ? data[item].trim()
        : data[item],
    };
  });

  return tempData;
}

function __openWhatsapp(number) {
  Linking.openURL(`https://wa.me/+91${number}`);
}
function __sendSMS(number) {
  Linking.openURL(`sms:${number}`);
}
function __doCall(number) {
  Linking.openURL(`tel:${number}`);
}
function __sendMail(email) {
  Linking.openURL(`mailto:${email}`);
}
function __getYearList(date) {
  let yearArray = [];
  for (let i = 2021; i <= date.getFullYear(); i++) {
    yearArray.push({year: i + ''});
  }
  return yearArray;
}
function __getCurrentYearMonth() {
  const monthList = [
    {month: 'January', number: 1},
    {month: 'February', number: 2},
    {month: 'March', number: 3},
    {month: 'April', number: 4},
    {month: 'May', number: 5},
    {month: 'June', number: 6},
    {month: 'July', number: 7},
    {month: 'August', number: 8},
    {month: 'september', number: 9},
    {month: 'October', number: 10},
    {month: 'November', number: 11},
    {month: 'December', number: 12},
  ];
  let tempMonth = [];
  for (let i = 0; i <= new Date().getMonth(); i++) {
    tempMonth.push(monthList[i]);
  }
  return tempMonth;
}
function __getTimeArray(date, isUpcoming = true) {
  let timeArray = [];
  timeArray.push({time: 'Today', date: date, key: ''});
  timeArray.push({
    time: 'This Week',
    startDate: moment()
      .clone()
      .startOf('isoWeek'),
    endDate: moment()
      .clone()
      .endOf('isoWeek'),
    key: 'this_week',
  });

  timeArray.push({
    time: 'Last Week',
    startDate: moment()
      .clone()
      .startOf('isoWeek')
      .add(-7, 'days'),
    endDate: moment()
      .clone()
      .startOf('isoWeek')
      .add(-1, 'days'),
    key: 'last_week',
  });
  timeArray.push({
    time: 'This Month',
    month: date,
    key: 'this_month',
  });
  timeArray.push({
    time: 'Last Month',
    month: new Date().setMonth(date.getMonth() - 1),
    key: 'last_month',
  });
  timeArray.push({
    time: 'This Year',
    year: date,
    key: 'this_year',
  });
  timeArray.push({
    time: 'Last Year',
    year: new Date().setFullYear(date.getFullYear() - 1),
    key: 'last_year',
  });
  if (isUpcoming) {
    timeArray.push({
      time: 'Upcoming',
      key: 'upcoming',
    });
  }
  return timeArray;
}

function __MyLog(data) {
  isLogDisplay && console.log(data);
}

const __findCommonElement = (array1, array2) => {
  for (let i = 0; i < array1.length; i++) {
    for (let j = 0; j < array2.length; j++) {
      if (array1[i] === array2[j]) {
        return true;
      }
    }
  }
};

export {
  __filterProfile,
  __selectDeselect,
  __getYears,
  __localSearch,
  __addDash,
  __addBlank,
  __openWhatsapp,
  __sendSMS,
  __doCall,
  __sendMail,
  __createAddress,
  __arrangeSwipableData,
  __getYearList,
  __getCurrentYearMonth,
  __getTimeArray,
  __MyLog,
  __findCommonElement,
};
