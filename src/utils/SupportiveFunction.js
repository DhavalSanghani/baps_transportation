function __sorting(data, sortBy) {
    const SortedMemberList = data.sort(function (a, b) {
        if (sortBy === 'first_name') {
            if (
                a.first_name.trim().toLowerCase() <
                b.first_name.trim().toLowerCase()
            )
                return -1;
            if (
                a.first_name.trim().toLowerCase() >
                b.first_name.trim().toLowerCase()
            )
                return 1;
        } else if (sortBy === 'last_name') {
            if (
                a.last_name.trim().toLowerCase() < b.last_name.trim().toLowerCase()
            )
                return -1;
            if (
                a.last_name.trim().toLowerCase() > b.last_name.trim().toLowerCase()
            )
                return 1;
        } else if (sortBy === 'mandal') {
            if (
                a.mandal_name.trim().toLowerCase() < b.mandal_name.trim().toLowerCase()
            )
                return -1;
            if (
                a.mandal_name.trim().toLowerCase() > b.mandal_name.trim().toLowerCase()
            )
                return 1;
        }
        else if (sortBy === 'xetra') {
            if (
                a.kshetra_name.trim().toLowerCase() < b.kshetra_name.trim().toLowerCase()
            )
                return -1;
            if (
                a.kshetra_name.trim().toLowerCase() > b.kshetra_name.trim().toLowerCase()
            )
                return 1;
        }
        else if (sortBy === 'role') {
            if (
                a.role_name.trim().toLowerCase() < b.role_name.trim().toLowerCase()
            )
                return -1;
            if (
                a.role_name.trim().toLowerCase() > b.role_name.trim().toLowerCase()
            )
                return 1;
        }
        return 0;
    });
    return SortedMemberList
}

export {
    __sorting
}