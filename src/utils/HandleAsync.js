import AsyncStorage from '@react-native-async-storage/async-storage';

const asyncAccess = {
  CURRENT_USER: '@currentUser',
  CURRENT_USER_PROFILE: '@currentUserProfile',
};
const cacheKeyword = {
  CACHE_HOME_SCREEN: '@CACHE_HOME_SCREEN',
  CACHE_MEMBER_TAB_COUNTER: '@CACHE_MEMBER_TAB_COUNTER',
  CACHE_CUSTOM_LIST: '@CACHE_CUSTOM_LIST',
  CACHE_MEMBER_LIST: '@CACHE_MEMBER_LIST',
  CACHE_MANDAL_SEVAK_LIST: '@CACHE_MANDAL_SEVAK_LIST',
  CACHE_SAMPARKKARYAKAR_LIST: '@CACHE_SAMPARKKARYAKAR_LIST',
  CACHE_REMINDER_LIST: '@CACHE_REMINDER_LIST',
  CACHE_REMINDER_TEMPLATES_LIST: '@CACHE_REMINDER_TEMPLATES_LIST',
  CACHE_NOTIFICATION: '@CACHE_NOTIFICATION',
};

const __clearAllCache = async () => {
  await AsyncStorage.multiRemove([
    asyncAccess.CURRENT_USER,
    asyncAccess.CURRENT_USER_PROFILE,
    cacheKeyword.CACHE_CUSTOM_LIST,
    cacheKeyword.CACHE_MEMBER_LIST,
    cacheKeyword.CACHE_MANDAL_SEVAK_LIST,
    cacheKeyword.CACHE_SAMPARKKARYAKAR_LIST,
    cacheKeyword.CACHE_REMINDER_LIST,
    cacheKeyword.CACHE_MEMBER_TAB_COUNTER,
    cacheKeyword.CACHE_NOTIFICATION
  ]);
};

export { cacheKeyword, asyncAccess, __clearAllCache };
