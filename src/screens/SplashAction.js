import React from 'react';
import { View } from 'react-native';
import { connect, useDispatch, useSelector } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import { StackActions } from '@react-navigation/native';
import FastImage from 'react-native-fast-image';
import { windowWidth } from '../constants';
import { splashLogo } from '../assets/icons';
import colors from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';

function SplashAction({ navigation, user }) {
  React.useEffect(() => {
    checkAuth();
  }, []);

  // const userInfo = useSelector(state => state.user);

  const checkAuth = async () => {
    // console.log('tt-->', userInfo);
    const user = await AsyncStorage.getItem('user');

    if (user == null) {
      navigation.dispatch(StackActions.replace('AuthStack'));
    } else {
      navigation.dispatch(StackActions.replace('UserStack'));
    }
    // if (userInfo.user !== 0) {
    //   navigation.dispatch(StackActions.replace('UserStack'));
    //   setTimeout(() => {
    //     SplashScreen.hide();
    //   }, 500);
    // } else {
    //   navigation.dispatch(StackActions.replace('AuthStack'));
    //   setTimeout(() => {
    //     SplashScreen.hide();
    //   }, 500);
    // }
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        backgroundColor: colors.white,
        alignItems: 'center',
      }}>
      <FastImage
        source={splashLogo}
        style={{ width: windowWidth * 0.5, aspectRatio: 1 }}
        resizeMode="contain"
      />
    </View>
  );
}

export default SplashAction;
