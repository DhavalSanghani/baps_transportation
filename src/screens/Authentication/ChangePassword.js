import React from 'react';
import {
  View,
  TouchableOpacity,
  SafeAreaView,
  KeyboardAvoidingView,
  StyleSheet,
  Keyboard,
} from 'react-native';

import { CTHeader } from '@components/CTHeader';
import colors from '@constants/colors';
import { CTButton } from '@components/Button';
import { CTInputField } from '@components/CTInputField';
import { CTIcon } from '@components/CTIcon';
import { closeIcon, eyeOffIcon, eyeOnIcon } from '../../assets/icons';

export default (ChangePassword = ({ navigation }) => {

  const [currentPassword, setCurrentPassword] = React.useState('');
  const [newPassword, setNewPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');

  const [showCurrentPassword, setShowCurrentPassword] = React.useState(false);
  const [showNewPassword, setShowNewPassword] = React.useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = React.useState(false);

  const _submit = async () => {
    // if (newPassword !== confirmPassword)
    //   return SimpleToast('Passwords do not match');
    // else if (currentPassword === newPassword)
    //   return SimpleToast('Current password and New password are same!');

    // dispatch(rootLoader(true));
    // let result = await resetPassword({
    //   member_id: userInfo.member_id,
    //   current_password: currentPassword,
    //   new_password: newPassword,
    // });
    // if (result?.status) {
    //   SimpleToast('Your password updated successfully');
    //   navigation.goBack();
    // } else {
    //   SimpleToast(result?.message);
    // }
    // dispatch(rootLoader(false));
  };

  return (
    <View style={{ flex: 1, backgroundColor: colors.white }}>
      <SafeAreaView />

      <CTHeader
        title="Change Password"
        headerTitleStyle={{ textAlign: 'left' }}
        isLeftBlank
        headerContainerStyle={{ marginLeft: 20 }}
        rightComp={
          <CTIcon
            source={closeIcon}
            onPress={() => navigation.goBack()}
            iconStyle={{ width: 24, height: 24 }}
          />
        }
      />
      <View style={{ height: 24 }} />
      <View style={{ paddingHorizontal: 20, flex: 1 }}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{ flex: 1 }}>
          <TouchableOpacity
            activeOpacity={1}
            style={{ flex: 1 }}
            onPress={() => Keyboard.dismiss()}>
            <View>
              <CTInputField
                secureTextEntry={!showCurrentPassword}
                rightSource={showCurrentPassword ? eyeOnIcon : eyeOffIcon}
                rightOnPress={() =>
                  setShowCurrentPassword(!showCurrentPassword)
                }
                rightIconStyle={styles.rightIconStyle}
                inputStyle={styles.inputStyle}
                value={currentPassword}
                onChangeText={setCurrentPassword}
                placeholder="Current Password"
                title="Current Password"
              />
              <View style={styles.itemSeparatorStyle} />
              <CTInputField
                secureTextEntry={!showNewPassword}
                rightSource={showNewPassword ? eyeOnIcon : eyeOffIcon}
                rightOnPress={() => setShowNewPassword(!showNewPassword)}
                inputStyle={styles.inputStyle}
                rightIconStyle={styles.rightIconStyle}
                value={newPassword}
                onChangeText={setNewPassword}
                placeholder="New Password"
                title="New Password"
              />
              <View style={styles.itemSeparatorStyle} />
              <CTInputField
                secureTextEntry={!showConfirmPassword}
                rightSource={showConfirmPassword ? eyeOnIcon : eyeOffIcon}
                rightOnPress={() =>
                  setShowConfirmPassword(!showConfirmPassword)
                }
                rightIconStyle={styles.rightIconStyle}
                inputStyle={styles.inputStyle}
                value={confirmPassword}
                onChangeText={setConfirmPassword}
                placeholder="Confirm Password"
                title="Confirm Password"
              />

              <View style={{ height: 20 }} />
              <CTButton
                fullPress={_submit}
                title="Update Password"
              />
            </View>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </View>
    </View>
  );
});
const styles = StyleSheet.create({
  inputStyle: {
    color: colors.black,
  },
  itemSeparatorStyle: {
    height: 30,
  },
  // rightIconStyle: { width: 16, height: 16 },
});
