import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  Modal,
  FlatList,
  ScrollView,
  StatusBar,
  Keyboard,
} from 'react-native';
import colors from '../../constants/colors';
import { SafeAreaView } from 'react-native';
import SimpleToast from '../../components/SimpleToast';
import { CTButton } from '../../components/Button';
import { CTInputField } from '../../components/CTInputField';
import { eyeOffIcon, eyeOnIcon } from '../../assets/icons';
import { useDispatch } from 'react-redux';
import { fonts } from '../../constants/fonts';
import { StackActions } from '@react-navigation/native';
import AuthHeader from '../../components/AuthHeader';
import fontSizes from '../../constants/fontSizes';
import { setUser } from '../../store/actions/user.action';
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginScreen = ({ navigation }) => {
  const dispatch = useDispatch();

  const [userName, setUserName] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [showPassword, setShowPassword] = React.useState(false);
  const [isButtonDisable, setIsButtonDisable] = React.useState(true);

  useEffect(() => {
    userName && password ? setIsButtonDisable(false) : setIsButtonDisable(true);
  }, [userName, password]);

  const _submit = async () => {
    Keyboard.dismiss();

    dispatch(setUser('Testing'));
    await AsyncStorage.setItem('user', 'testing');
    navigation.dispatch(StackActions.replace('UserStack'));
  };

  return (
    <>
      <SafeAreaView style={{ backgroundColor: colors.primary }} />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' && 'padding'}
        style={{ flex: 1, backgroundColor: colors.white }}
        keyboardVerticalOffset={StatusBar.currentHeight}>
        <ScrollView
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          bounces={false}>
          <AuthHeader>
            <View style={styles.loginContainerStyle}>
              <View
                style={{
                  paddingHorizontal: 30,
                  paddingTop: 36,
                  flex: 1,
                }}>
                <Text style={styles.titleStyle}>Welcome to Transport App</Text>
                <Text style={styles.decriptionStyle}>
                  Enter your credentials to continue
                </Text>
                <View
                  style={{
                    width: 25,
                    height: 3,
                    backgroundColor: colors.primary,
                    marginVertical: 18,
                    borderRadius: 10,
                  }}
                />
                <View style={{ marginVertical: 4 }}>
                  <CTInputField
                    containerStyle={{ marginVertical: 10 }}
                    value={userName}
                    placeholder="Email ID "
                    title="Email ID"
                    onChangeText={setUserName}
                    keyboardType="email-address"
                  />

                  <CTInputField
                    secureTextEntry={!showPassword}
                    rightSource={showPassword ? eyeOnIcon : eyeOffIcon}
                    rightOnPress={() => setShowPassword(!showPassword)}
                    containerStyle={{ marginTop: 20 }}
                    value={password}
                    placeholder="Password"
                    title="Password"
                    onChangeText={setPassword}
                  />
                  <View style={{ height: 24 }} />
                  <CTButton
                    disabled={isButtonDisable}
                    fullPress={_submit}
                    title="Login to your account"
                  />
                  <View style={{ height: 10 }} />
                  {/* <CTButton
                    fullPress={() => navigation.navigate('ForgotPassword')}
                    contentContainerStyle={{ backgroundColor: colors.white }}
                    titleStyle={{
                      color: colors.black,
                      fontFamily: fonts.SFProTextRegular,
                    }}
                    title="Forgot Password?"
                  /> */}
                  {/* {showTester && (
                    <CTButton
                      fullPress={() => setIsModalVisible(true)}
                      titleStyle={{color: colors.black}}
                      title="Test Users"
                    />
                  )}
                  {!isLive && <Text>local</Text>} */}
                </View>
              </View>
            </View>
          </AuthHeader>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  loginContainerStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  titleStyle: {
    fontSize: fontSizes.f18,
    fontFamily: fonts.SFProTextBold,
  },
  decriptionStyle: {
    marginTop: 6,
    fontFamily: fonts.SFProTextRegular,
  },
});


// import React from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { StackActions } from '@react-navigation/native';
// import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
// import { connect } from 'react-redux';
// import colors from '../../constants/colors';
// import { setUser } from '../../store/actions/user.action';
// import AsyncStorage from '@react-native-async-storage/async-storage';

// const LoginScreen = ({ navigation, props }) => {
//   const dispatch = useDispatch();
//   const { user } = useSelector(({ userReducer }) => userReducer);
//   console.log('start-->', user);

//   const login = async () => {
//     dispatch(setUser('Testing'));
//     await AsyncStorage.setItem('user', 'testing');
//     navigation.dispatch(StackActions.replace('UserStack'));
//   };

//   return (
//     <View style={styles.loginContainerStyle}>
//       <Text>Login Screen</Text>
//       <TouchableOpacity style={styles.btn} onPress={() => login()}>
//         <Text>Login</Text>
//       </TouchableOpacity>
//     </View>
//   );
// };

// // const mapDispatchToProps = dispatch => {
// //   return {
// //     setUser: user => dispatch(setUser(user)),
// //   };
// // };

// // const mapStateToProps = state => {
// //   return {
// //     user: state.user,
// //   };
// // };

// export default LoginScreen;

// // export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

// const styles = StyleSheet.create({
//   loginContainerStyle: {
//     flex: 1,
//     backgroundColor: colors.white,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   btn: {
//     backgroundColor: 'pink',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
