import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList, Image } from 'react-native';
import { CTHeader } from '@components/CTHeader';
import colors from '../../../../constants/colors';
import { fonts } from '../../../../constants/fonts';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CTActionSheet from '../../../../components/CTActionSheet';
import { CTSubHeadingLeft } from '../../../../components/CTHeading';
import CTCheckBox from '../../../../components/CTCheckBox';
import { CTButton } from '../../../../components/Button';
class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortOptionVisible: false,
      statusArray: [
        {
          name: 'All',
        },
        {
          name: 'Read',
        },
        {
          name: 'Unread',
        },
      ],
      typeArray: [
        {
          name: 'All',
        },
        {
          name: 'Fuel',
        },
        {
          name: 'Security',
        },
      ],
      monthArray: [
        {
          name: 'All',
        },
        {
          name: 'May 22',
        },
        {
          name: 'April 22',
        },
      ],
      selectStatus: 'All',
      selectType: 'All',
      selectMonth: 'All',
      data: [
        {
          title: 'Fuel request is rejected',
          desc: 'vehicle number (GJ05 4582) request rejected 35 ml fuels filled ',
          role: 'Fuel desk admin',
          date: '22-05-2022',
          time: '9:55 pm',
          status: 'unread'
        },
        {
          title: 'Fuel filled successfully',
          desc: 'vehicle number (GJ05 4582) request accept',
          role: 'admin',
          date: '22-05-2022',
          time: '9:55 pm',
          status: 'unread'
        },
        {
          title: 'Fuel filled successfully',
          desc: 'vehicle number (GJ05 4582) request accept',
          role: 'admin',
          date: '22-05-2022',
          time: '9:55 pm',
          status: 'unread'
        },
        {
          title: 'Fuel request is rejected',
          desc: 'vehicle number (GJ05 4582) request rejected',
          role: 'Fuel desk admin',
          date: '22-05-2022',
          time: '9:55 pm',
          status: 'read'
        },
        {
          title: 'Fuel request is rejected',
          desc: 'vehicle number (GJ05 4582) request rejected',
          role: 'Fuel desk admin',
          date: '22-05-2022',
          time: '9:55 pm',
          status: 'read'
        }
      ]
    };
  }

  renderItem = ({ item }) => {
    return (
      <View style={styles.notificationView}>
        <Image
          source={require('../../../../assets/icons/fuel.png')}
          style={styles.icon}
        />
        <View style={styles.rightView}>
          <Text style={[styles.titleTxt, {
            color: item.status == 'read' ? colors.darkGrayDisable : 'black'
          }]}>{item.title}</Text>
          <Text style={[styles.descTxt, {
            color: item.status == 'read' ? colors.darkGrayDisable : 'black'
          }]}>{item.desc}</Text>
          <View style={styles.lastRow}>
            <Text style={styles.roleTxt}>{item.role}</Text>
            <Text style={styles.bulletPoint}>{'\u2B24'}</Text>
            <Text style={styles.roleTxt}>{item.date}</Text>
            <Text style={[styles.roleTxt, {
              marginLeft: 8
            }]}>@ {item.time}</Text>
          </View>
        </View>
      </View>
    )
  }

  filter = () => {
    let { sortOptionVisible } = this.state;

    this.setState({
      sortOptionVisible: !sortOptionVisible
    })
  }

  statusRenderItem = ({ item, index }) => {
    let { selectStatus } = this.state;

    return (
      <>
        <View style={{ flexDirection: 'row' }}>
          <CTCheckBox
            selected={selectStatus == item.name}
            title={item.name}
            onPress={() => {
              this.setState({
                selectStatus: item.name
              })
            }}
          />
        </View>
      </>
    )
  }

  typeRenderItem = ({ item, index }) => {
    let { selectType } = this.state;

    return (
      <>
        <View style={{ flexDirection: 'row' }}>
          <CTCheckBox
            selected={selectType == item.name}
            title={item.name}
            onPress={() => {
              this.setState({
                selectType: item.name
              })
            }}
          />
        </View>
      </>
    )
  }

  monthRenderItem = ({ item, index }) => {
    let { selectMonth } = this.state;

    return (
      <>
        <View style={{ flexDirection: 'row' }}>
          <CTCheckBox
            selected={selectMonth == item.name}
            title={item.name}
            onPress={() => {
              this.setState({
                selectMonth: item.name
              })
            }}
          />
        </View>
      </>
    )
  }

  onPressApplyFilters = () => {
    let { selectMonth, selectStatus, selectType } = this.state;

    this.filter();
  }

  render() {
    let { data, sortOptionVisible, statusArray, typeArray, monthArray } = this.state;

    return (
      <View style={styles.container}>
        <CTHeader
          title="All Notifications"
          headerTitleStyle={{ textAlign: 'left' }}
          isLeftBlank
          headerContainerStyle={{ marginLeft: 20 }}
        />
        <View style={styles._horizontalLine} />
        <View style={styles.topRow}>
          <Text style={styles.topTxt}>Showing 5 unread messages</Text>
          <View style={[styles.iconView, {
            right: 40
          }]}>
            <Ionicons
              name='mail-outline'
              size={14}
              color={colors.black}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.filter()}
            style={[styles.iconView, {
              right: 0
            }]}>
            <Ionicons
              name='funnel-outline'
              size={14}
              color={colors.black}
            />
          </TouchableOpacity>
        </View>
        <View style={styles._horizontalLine} />
        <View>
          <FlatList
            data={data}
            renderItem={this.renderItem}
            ItemSeparatorComponent={() => <View style={[styles._horizontalLine, {
              width: '80%'
            }]} />}
          />
        </View>
        <CTActionSheet
          // selectedOption={selectedSorting}
          isVisible={sortOptionVisible}
          onBackdropPress={this.filter}
          onCancelPress={this.filter}
          children={
            <>
              <CTHeader
                title="Filter"
                headerTitleStyle={{ textAlign: 'left' }}
                isLeftBlank
                headerContainerStyle={{
                  marginLeft: 10,
                  marginBottom: 10
                }}
              />
              <CTSubHeadingLeft title="Notification Status" titleStyle={{
                fontSize: 14,
                fontFamily: fonts.SFProTextBold,
                marginLeft: -10,
              }} />
              <FlatList
                keyboardShouldPersistTaps="always"
                bounces={false}
                horizontal
                keyExtractor={(item, index) => String(index)}
                data={statusArray}
                renderItem={this.statusRenderItem}
              />
              <CTSubHeadingLeft title="Notification Type" titleStyle={{
                fontSize: 14,
                fontFamily: fonts.SFProTextBold,
                marginLeft: -10,
                marginTop: 20
              }} />
              <FlatList
                keyboardShouldPersistTaps="always"
                bounces={false}
                horizontal
                keyExtractor={(item, index) => String(index)}
                data={typeArray}
                renderItem={this.typeRenderItem}
              />
              <CTSubHeadingLeft title="Month" titleStyle={{
                fontSize: 14,
                fontFamily: fonts.SFProTextBold,
                marginLeft: -10,
                marginTop: 20
              }} />
              <FlatList
                keyboardShouldPersistTaps="always"
                bounces={false}
                horizontal
                keyExtractor={(item, index) => String(index)}
                data={monthArray}
                renderItem={this.monthRenderItem}
              />
              <CTButton
                contentContainerStyle={{ width: '100%', marginTop: 25 }}
                title="Apply Filters"
                fullPress={this.onPressApplyFilters}
              />
            </>
          }
        />
      </View>
    );
  }
}

export default Notifications;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  _horizontalLine: {
    alignSelf: 'center',
    width: '90%',
    height: 1,
    backgroundColor: '#efefef',
  },
  icon: {
    width: 30,
    height: 30
  },
  notificationView: {
    paddingHorizontal: 20,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleTxt: {
    fontFamily: fonts.SFProTextBold,
    color: colors.black,
    fontSize: 14,
    marginRight: 6,
  },
  descTxt: {
    fontFamily: fonts.SFProTextMedium,
    color: colors.black,
    fontSize: 12,
    marginRight: 6,
    marginTop: 4
  },
  rightView: {
    marginLeft: 15,
  },
  roleTxt: {
    fontFamily: fonts.SFProTextMedium,
    color: colors.darkGrayDisable,
    fontSize: 12,
  },
  lastRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 4,
    flexWrap: 'wrap'
  },
  bulletPoint: {
    fontSize: 6,
    marginLeft: 6,
    marginRight: 4
  },
  topTxt: {
    fontFamily: fonts.SFProTextBold,
    color: colors.black,
    fontSize: 12,
  },
  iconView: {
    width: 28,
    height: 28,
    borderColor: '#efefef',
    borderWidth: 1,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  topRow: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 15
  },
  horizontalListSpaceStyle: {
    width: 10,
  },
});
