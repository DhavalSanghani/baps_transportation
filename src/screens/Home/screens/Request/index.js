import React, { useState, useRef, useEffect } from 'react';
import {
  View,
  Text,
  BackHandler,
  ToastAndroid,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Animated,
  FlatList
} from 'react-native';
import { CTIcon } from '../../../../components/CTIcon';
import commonStyles from '../../../../constants/commonStyles';
import {
  bottomDropdownArrowIcon,
  leftArrowIcon,
  notificationIcon,
  rightArrowIcon,
  upDropdownArrowIcon,
  addIcon,
} from '../../../../assets/icons';
import colors from '../../../../constants/colors';
import fontSizes from '../../../../constants/fontSizes';
import { fonts } from '../../../../constants/fonts';
import CTShadow from '../../../../components/CTShadow';
import { CTAnimatedDropDown } from '../../../../components/CTAnimatedDropDown';
import { CTMapList } from '../../../../components/CTMapList';
import SortProfile from './SortProfile';
import { isIOS } from '../../../../constants';

export default Request = ({ navigation }) => {
  const FUEL_ARRAY = [
    {
      status: 'APPROVED',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
    {
      status: 'REJECTED',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
    {
      status: 'PENDING',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
    {
      status: 'APPROVED',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
    {
      status: 'PENDING',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
    {
      status: 'REJECTED',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
    {
      status: 'PENDING',
      vehicle_no: 'GJ LB 4518',
      date: '27 Apr',
      phone: '9827025478',
      fuelQuantity: '80 Ltr',
      fuelType: 'Desel',
    },
  ];
  const duration = 150;
  const [menuList, setMenuList] = useState([
    {
      key: 'All Requests',
      title: 'All Requests',
      total: 0,
      isAccess: true,
    },
    {
      key: 'Approved Requests',
      title: 'Approved Requests',
      total: 0,
      isAccess: true,
    },
    {
      key: 'Pending Requests',
      title: 'Pending Requests',
      total: 0,
      isAccess: true,
    },
    {
      key: 'Rejected Requests',
      title: 'Rejected Requests',
      total: 0,
      isAccess: true,
    },
  ]);
  const [isRouteSet, setIsRouteSet] = React.useState(false);

  const [isVisibleProfile, setIsVisibleProfile] = React.useState(false);
  const [tempIsVisibleProfile, setTempIsVisibleProfile] = React.useState(false);

  const [viewProfile, setviewProfile] = React.useState(null);

  const floatAnimation = useRef(new Animated.Value(0)).current;
  const [selectedMenu, setSelectedMenu] = useState(0);
  const [isOpen, setIsOpen] = useState(false);

  React.useEffect(() => {
    setSelectedMenu(selectedMenu);
  }, [selectedMenu]);

  useEffect(() => {
    loadTabCounters();
  }, []);

  const loadTabCounters = async () => {
    // let cacheResult = await AsyncStorage.getItem(
    //   cacheKeyword.CACHE_MEMBER_TAB_COUNTER,
    // );
    // if (cacheResult) {
    //   cacheResult = JSON.parse(cacheResult);
    //   setMenuList([...setMenuTabCounter(cacheResult)]);
    //   setIsRouteSet(true);
    // }
    // const result = await getTabCounter({});

    // if (result.status) {
    const hData = [];
    setIsRouteSet(true);
    setMenuList([...setMenuTabCounter()]);
    // await AsyncStorage.setItem(
    //   cacheKeyword.CACHE_MEMBER_TAB_COUNTER,
    //   JSON.stringify(hData),
    // );
    // }
  };
  const setMenuTabCounter = () => {
    let hMenuList = menuList;
    hMenuList = hMenuList.map((item) => {
      if (item.title === 'All Requests') {
        return { ...item, total: 78 };
      } else if (item.title === 'Approved Requests') {
        return { ...item, total: 45 };
      } else if (item.title === 'Pending Requests') {
        return { ...item, total: 89 };
      } else if (item.title === 'Rejected Requests') {
        return { ...item, total: 12 };
      }
    });
    return hMenuList;
  };

  const toggleOpen = () => {
    setIsOpen((value) => !value);
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
  };
  const renderMenuItem = ({ item, index }) => {
    if (selectedMenu === index) {
      return null;
    }
    return (
      <TouchableOpacity
        style={{ paddingVertical: 10 }}
        onPress={() => {
          toggleOpen();
          setSelectedMenu(index);
        }}
      >
        <Text
          style={styles.headerTextStyle}
        >{`${item.title} (${item.total})`}</Text>
      </TouchableOpacity>
    );
  };

  const renderTabBar = (props) => {
    return (
      <View style={[isOpen && { zIndex: 1000 }]}>
        <CTShadow
          style={{
            ...commonStyles.topTabNavigatorContainerStyle,
            backgroundColor: 'white',
          }}
        >
          <Animated.View
            style={[
              styles.headerContainerStyle,
              {
                marginTop: floatAnimation,
              },
              isOpen && { backgroundColor: 'white' },
            ]}
          >
            <View style={styles.dropDownContainerStyle}>
              <CTAnimatedDropDown
                isOpen={isOpen}
                toggleOpen={toggleOpen}
                title={
                  <TouchableOpacity
                    disabled={menuList.length === 1}
                    onPress={toggleOpen}
                    style={[styles.headerStyle]}
                  >
                    <Text style={styles.headerTextStyle}>
                      {`${menuList.filter((item) => item.isAccess === true)[
                        selectedMenu
                      ].title
                        } (${menuList.filter((item) => item.isAccess === true)[
                          selectedMenu
                        ].total
                        })`}
                    </Text>
                    {menuList.length !== 1 && (
                      <CTIcon
                        disabled
                        source={
                          !isOpen
                            ? bottomDropdownArrowIcon
                            : upDropdownArrowIcon
                        }
                        iconStyle={{
                          marginLeft: 4,
                          width: 20,
                          height: 20,
                        }}
                      />
                    )}
                  </TouchableOpacity>
                }
                children={
                  <CTMapList
                    data={menuList.filter((item) => item.isAccess === true)}
                    renderItem={renderMenuItem}
                  />
                }
              />
            </View>
          </Animated.View>
        </CTShadow>
      </View>
    );
  };

  const leftIconPress = () => {
    console.log('left--->');
  };

  const rightIconPress = () => {
    console.log('right--->');
  };

  const getDateFormate = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'space-around',
            flexDirection: 'row',
            padding: 12,

            backgroundColor: colors.white,
          }}
        >
          <CTIcon
            source={leftArrowIcon}
            iconStyle={{ width: 12, height: 12, tintColor: colors.black }}
            onPress={leftIconPress}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate('ChangePassword')}
            style={styles.buttonContainerStyle}
          >
            <Text style={[styles.headerTextStyle]}>Apr 2022</Text>
          </TouchableOpacity>
          <CTIcon
            source={rightArrowIcon}
            iconStyle={{ width: 12, height: 12, tintColor: colors.black }}
            onPress={rightIconPress}
          />
        </View>
      </View>
    );
  };

  const reportCard = () => {
    return (
      <View style={{ marginVertical: 10 }}>
        <View style={styles._horizontalLine} />

        <View style={{ flexDirection: 'row' }}>
          <View style={styles.reportContainer}>
            <View style={styles.reportStyle}>
              <Text style={styles.txtBold}>04</Text>
              <Text style={styles.txtTitle}>Approved</Text>
            </View>
            <View style={styles.verticalLine} />
            <View style={styles.reportStyle}>
              <Text style={styles.txtBold}>00</Text>
              <Text style={styles.txtTitle}>Pending</Text>
            </View>
            <View style={styles.verticalLine} />
            <View style={styles.reportStyle}>
              <Text style={styles.txtBold}>01</Text>
              <Text style={styles.txtTitle}>Rejected</Text>
            </View>
          </View>
        </View>
        <View style={styles._horizontalLine} />
      </View>
    );
  };

  const isCheckStatus = (status) => {
    if (status == 'APPROVED') {
      return '#E3FBEE';
    } else if (status == 'REJECTED') {
      return '#FFE7E1';
    } else {
      return '#FFF2D7';
    }
  };

  const isCheckTextStatus = (status) => {
    if (status == 'APPROVED') {
      return '#56C288';
    } else if (status == 'REJECTED') {
      return '#FF967F';
    } else {
      return '#FAB347';
    }
  };

  const genderRenderItem = ({ item, index }) => {
    return (
      <View style={{ marginHorizontal: 12, marginVertical: 5 }}>
        <TouchableOpacity
          key={String(index)}
          onPress={() => {
            setviewProfile(item);
            setIsVisibleProfile(true);
          }}
        >
          <View style={styles.listContainer}>
            <View
              style={{
                justifyContent: 'flex-start',
                backgroundColor: isCheckStatus(item?.status),
                paddingVertical: 4,
                paddingHorizontal: 12,
                borderRadius: 12,
              }}
            >
              <Text
                style={[
                  styles.txtStatus,
                  { color: isCheckTextStatus(item?.status) },
                ]}
              >
                {item?.status}
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              paddingHorizontal: 12,
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 4,
            }}
          >
            <Text style={styles.headerTextStyle}>{item?.vehicle_no}</Text>
            <Text style={styles.headerTextStyle}>{item?.fuelQuantity}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: 4,
              paddingHorizontal: 12,
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.txtDate}>{item.date} </Text>
              <View
                style={{
                  width: 4,
                  height: 4,
                  borderRadius: 2,
                  marginHorizontal: 6,
                  alignSelf: 'center',
                  backgroundColor: '#36414C',
                }}
              ></View>
              <Text style={styles.txtDate}>{item.phone}</Text>
            </View>
            <Text style={styles.txtDate}>{item.fuelType}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.horizontalLine} />
      </View>
    );
  };

  return (
    <>
      <SortProfile
        isVisibleProfile={isVisibleProfile}
        setIsVisibleProfile={(data) => setIsVisibleProfile(data)}
        userInfo={viewProfile}
      // openCommunication={this.__openCommunication}
      // setTempIsVisibleProfile={(data) => setTempIsVisibleProfile(data)}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: colors.white,
          justifyContent: 'flex-start',
        }}
      >
        <SafeAreaView />
        {renderTabBar()}
        {getDateFormate()}
        {reportCard()}
        <View
          style={{ flex: isIOS() ? 1 : 0.75, backgroundColor: colors.white }}
        >
          <FlatList
            keyboardShouldPersistTaps="always"
            bounces={false}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => String(index)}
            data={FUEL_ARRAY}
            renderItem={genderRenderItem}
          />
        </View>
        <View
          style={{
            justifyContent: 'flex-start',
            alignItems: 'center',
            flex: 0.25,
            backgroundColor: colors.white,
            marginTop: 10,
          }}
        >
          <View style={{}}>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 15,
                paddingVertical: 8,
                paddingHorizontal: 16,
                flexDirection: 'row',
                backgroundColor: '#36414C',
              }}
              onPress={() => {
                navigation.navigate('AddRequest');
              }}
            >
              <CTIcon
                source={addIcon}
                iconStyle={{ width: 20, height: 20, tintColor: colors.white }}
                onPress={leftIconPress}
              />
              <Text style={styles.txtAdd}> Add Request </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: 15,
  },
  txtBold: {
    color: colors.black,
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextBold,
  },
  txtTitle: {
    color: '#66737F',
    fontSize: fontSizes.f12,
    fontFamily: fonts.SFProTextSemiBold,
  },
  txtDate: {
    color: '#66737F',
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextSemiBold,
  },
  txtStatus: {
    fontSize: fontSizes.f12,
    fontFamily: fonts.SFProTextSemiBold,
  },
  txtAdd: {
    textAlign: 'center',
    color: colors.white,
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextBold,
  },
  reportContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 12,
    justifyContent: 'space-around',
    backgroundColor: colors.white,
  },
  reportStyle: { flex: 1, alignItems: 'center' },
  horizontalLine: {
    marginTop: 10,
    borderTopColor: '#efefef',
    borderTopWidth: 0.5,
  },
  headerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  _horizontalLine: {
    alignSelf: 'center',
    width: '90%',
    height: 1,
    backgroundColor: '#efefef',
  },
  verticalLine: { width: 1, height: '80%', backgroundColor: '#efefef' },
  listContainer: {
    alignItems: 'flex-start',
  },
  dropDownContainerStyle: {
    paddingTop: 15,
    paddingBottom: 10,
  },
  headerTextStyle: {
    fontSize: fontSizes.f16,
    fontFamily: fonts.SFProTextBold,
  },
  headerContainerStyle: {
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  iconStyle: {
    width: 15,
    height: 15,
    marginHorizontal: 10,
  },

  notificationContainerStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});
