import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import CTBottomModal from '../../../../components/CTBottomModal';
import colors from '../../../../constants/colors';
import { fonts } from '../../../../constants/fonts';
import fontSizes from '../../../../constants/fontSizes';

export default function SortProfile({
  isVisibleProfile,
  setIsVisibleProfile,
  userInfo,
}) {
  const navigation = useNavigation();
  return (
    <CTBottomModal
      isVisible={isVisibleProfile}
      onCancelPress={() => {
        setIsVisibleProfile(false);
      }}
      onBackdropPress={() => {
        setIsVisibleProfile(false);
      }}
      itemContainerStyle={{ maxHeight: '85%' }}
      animationInTiming={200}
      animationOutTiming={200}
    >
      <ScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
        bounces={false}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: colors.white,
            // justifyContent: 'center',
            // alignItems: 'center',
          }}
        >
          <View>
            <Text style={styles.darkTitleStyle}>Notes</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={styles.viewContainer}>
              <Text style={styles.txtDesc}>{'Vehicle No.'}</Text>
              <Text style={styles.txtTitle}>{'Gj 05 HS 05845'}</Text>
            </View>
            <View style={styles.viewContainerss}>
              <Text style={styles.txtDesc}>{'Date'}</Text>
              <Text style={styles.txtTitle}>{'28-05-2022'}</Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={styles.viewContainer}>
              <Text style={styles.txtDesc}>{'Fuel Type'}</Text>
              <Text style={styles.txtTitle}>{'Diesel'}</Text>
            </View>
            <View style={styles.viewContainerss}>
              <Text style={styles.txtDesc}>{'Contact No.'}</Text>
              <Text style={styles.txtTitle}>{'95642123254'}</Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={styles.viewContainer}>
              <Text style={styles.txtDesc}>{'Request Qty'}</Text>
              <Text style={styles.txtTitle}>{'80 Ltr'}</Text>
            </View>
            <View style={styles.viewContainerss}>
              <Text style={styles.txtDesc}>{'Refill Qty'}</Text>
              <Text style={styles.txtTitle}>{'60 Ltr'}</Text>
            </View>
          </View>
          <View style={styles.viewContainer}>
            <Text style={styles.txtDesc}>Notes</Text>
            <Text style={styles.txtTitle}>
              You have requested fuel of 80 Ltr is not accepted. We have to fill
              fuel with 60 Ltr as per your daily quota limit.
            </Text>
          </View>
        </View>

        <View style={{ height: 20 }} />
      </ScrollView>
    </CTBottomModal>
  );
}

const styles = StyleSheet.create({
  areaDetailsContainerStyle: {
    width: '100%',
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 20,
  },
  viewContainer: {
    // flexDirection: 'row',
    marginTop: 10,
    flex: 0.5,
    // backgroundColor: 'green',
    // alignItems: 'center',
  },
  viewContainerss: {
    // flexDirection: 'row',
    marginTop: 16,
    flex: 0.5,
    // backgroundColor: 'blue',
    // alignItems: 'center',
  },

  itemSeparatorStyle: {
    height: '100%',
    width: 1.5,
    backgroundColor: colors.grayDisable,
  },
  txtTitle: {
    marginTop: 6,
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextSemiBold,
  },
  txtDesc: {
    // marginTop: 5,
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextMedium,
    color: '#95A2AE',
    alignItems: 'center',
  },
  lightTitleStyle: {
    color: colors.darkGrayDisable,
    fontSize: fontSizes.f12,
    fontFamily: fonts.SFProTextMedium,
  },
  darkTitleStyle: {
    color: colors.black,
    fontSize: fontSizes.f18,
    fontFamily: fonts.SFProTextBold,
    marginBottom: 20,
    // textAlign: 'center',
  },
  titleText: {
    fontSize: fontSizes.f12,
    fontFamily: fonts.SFProTextMedium,
    color: colors.lightGray,
  },
  valueText: {
    fontSize: fontSizes.f13,
    fontFamily: fonts.SFProTextMedium,
    color: colors.black,
    marginTop: 4,
  },
  contactContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  detailsContainerStyle: {
    alignItems: 'center',
  },
  linkIconContainerStyle: {
    marginLeft: 13,
    alignSelf: 'flex-end',
  },
});
