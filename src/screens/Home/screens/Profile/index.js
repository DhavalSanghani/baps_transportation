import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView,
} from 'react-native';
import { cameraIcon, galleryIcon, notificationIcon } from '../../../../assets/icons';
import CTActionSheet from '../../../../components/CTActionSheet';
import CTAvatar from '../../../../components/CTAvatar';
import colors from '@constants/colors';
import { mediaOption } from '@constants/env';
import { fonts } from '@constants/fonts';
import { __addDash, __filterProfile } from '../../../../utils/BasicFunctions';
import Setting from './ProfilePages/Setting';
import commonStyles from '@constants/commonStyles';
import { absoluteBtnHeight } from '@constants';
import fontSizes from '@constants/fontSizes';
import CTNotificationIcon from '../../../../components/CTNotificationIcon';
import { CTHeader } from '@components/CTHeader';

export default function Profile({ navigation }) {
    const [selectedIndex, setSelectedIndex] = useState(0);
    const [isPhotoPicker, setIsPhotoPicker] = useState(false);
    const [profilePic, setIsProfile] = useState('');
    const [profilePicLoader, setIsProfileLoader] = useState(false);

    const profilePicker = [
        {
            title: 'Choose from Photo Album',
            icon: galleryIcon,
            onPress: () => chooseMedia(mediaOption.gallery),
        },
        {
            title: 'Capture Photo',
            icon: cameraIcon,
            onPress: () => chooseMedia(mediaOption.camera),
        },
    ];

    const chooseMedia = async option => {
        // if (option === mediaOption.camera) {
        //     let res = await cameraPicker({ height: 1000, width: 1000 });
        //     if (res.status) {
        //         setIsPhotoPicker(false);
        //         setIsProfileLoader(true);
        //         const result = await dispatch(editUserPic({ profile_pic: res.data }));
        //         if (result.status) {
        //             setIsProfile(res.data.uri);
        //             SimpleToast('Profile pic has been updated');
        //         } else {
        //             SimpleToast(result?.data);
        //         }
        //         setIsProfileLoader(false);
        //     }
        // } else if (option === mediaOption.gallery) {
        //     let res = await photoPicker({ height: 1000, width: 1000 });
        //     if (res.status) {
        //         setIsPhotoPicker(false);
        //         setIsProfileLoader(true);
        //         const result = await dispatch(editUserPic({ profile_pic: res.data }));

        //         if (result.status) {
        //             setIsProfile(res.data.uri);
        //             SimpleToast('Profile pic has been updated');
        //         } else {
        //             SimpleToast(result?.data);
        //         }
        //         setIsProfileLoader(false);
        //     }
        // }
    };

    const onPressNotificationIcon = () => navigation.navigate('Notifications');

    return (
        <>
            <View style={styles.container}>
                <SafeAreaView />
                <CTHeader
                    title="Your Profile"
                    headerTitleStyle={{ textAlign: 'left' }}
                    isLeftBlank
                    headerContainerStyle={{ marginLeft: 20 }}
                />
                {/* <View style={styles.headerStyle}>
                    
                    <CTNotificationIcon
                        onPress={onPressNotificationIcon}
                    // unreadNotificationCounter={unreadNotificationCounter}
                    />
                </View> */}
                <ScrollView
                    // ref={scrollRef}
                    keyboardShouldPersistTaps="always"
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        paddingBottom: absoluteBtnHeight,
                    }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <CTAvatar
                            size={144}
                            source={{ uri: profilePic }}
                            resizeMode="cover"
                            style={commonStyles.profileAvatarStyle}
                            isLoader={profilePicLoader}
                            onPressEdit={() => setIsPhotoPicker(true)}
                        />
                        <View
                            style={{
                                width: 24,
                                height: 4,
                                backgroundColor: colors.primary,
                                marginVertical: 16,
                                borderRadius: 100,
                            }}
                        />
                        <View style={{ alignItems: 'center' }}>
                            <Text
                                style={{
                                    fontSize: fontSizes.f14,
                                    fontFamily: fonts.SFProTextBold,
                                    textAlign: 'center',
                                    color: 'black'
                                }}>
                                Testing abc
                            </Text>
                        </View>
                        <View style={styles.areaDetailsContainerStyle}>
                            <View style={{ alignItems: 'center', width: '30%' }}>
                                <Text style={styles.lightTitleStyle}>Department</Text>
                                <Text style={styles.darkTitleStyle}>
                                    Transport
                                </Text>
                            </View>
                            <View style={styles.itemSeparatorStyle} />
                            <View style={{ alignItems: 'center', width: '40%' }}>
                                <Text style={styles.lightTitleStyle}>Contact No.</Text>
                                <Text style={styles.darkTitleStyle}>
                                    9898098980
                                </Text>
                            </View>
                        </View>
                        <View style={styles._horizontalLine} />
                        {/* <CTTopTabNavigator
                            data={navigationListArray}
                            selectedIndex={selectedIndex}
                            onPressTab={setSelectedIndex}
                        /> */}
                        <View style={{ width: '100%' }}>
                            <Setting navigation={navigation} />
                        </View>
                    </View>
                </ScrollView>
            </View>
            <CTActionSheet
                data={profilePicker}
                isVisible={isPhotoPicker}
                onBackdropPress={setIsPhotoPicker}
                onCancelPress={setIsPhotoPicker}
            />
        </>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        paddingTop: 15,
    },
    headerStyle: {
        height: 50,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20,
    },
    lightTitleStyle: {
        color: colors.darkGrayDisable,
        fontSize: fontSizes.f11,
        fontFamily: fonts.SFProTextSemiBold,
    },
    darkTitleStyle: {
        marginTop: 4,
        color: colors.black,
        fontSize: fontSizes.f13,
        fontFamily: fonts.SFProTextBold,
        textAlign: 'center',
    },

    areaDetailsContainerStyle: {
        width: '100%',
        marginTop: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginBottom: 16,
    },
    itemSeparatorStyle: {
        height: '100%',
        width: 0.7,
        backgroundColor: colors.grayDisable,
    },
    itemBottomBoxStyle: {
        width: 24,
        height: 4,
        borderTopLeftRadius: 2,
        borderTopRightRadius: 2,
        backgroundColor: colors.primary,
        alignSelf: 'center',
        marginTop: 5,
    },
    _horizontalLine: {
        alignSelf: 'center',
        width: '90%',
        height: 1,
        backgroundColor: '#efefef',
    },
});
