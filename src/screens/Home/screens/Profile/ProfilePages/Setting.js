import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { CTButton } from '../../../../../components/Button';
import { CTIcon } from '../../../../../components/CTIcon';
import colors from '@constants/colors';
import { fonts } from '@constants/fonts';
import fontSizes from '@constants/fontSizes';
import { rightArrowIcon } from '../../../../../assets/icons';
import { CTHeadingBottom } from '../../../../../components/CTHeading';
import CTConfirmationBox from '../../../../../components/CTConfirmationBox';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StackActions } from '@react-navigation/native';

export default (Setting = ({ navigation }) => {
  const [isLogOut, setIsLogOut] = useState(false);
  const onLogout = async () => {
    await AsyncStorage.removeItem('user');
    navigation.dispatch(StackActions.replace('AuthStack'));
  };

  return (
    <View style={{ width: '100%', marginBottom: 10, paddingHorizontal: 20 }}>
      <Text style={styles.settingTitle}>General Settings</Text>
      <View style={styles._horizontalLine} />
      <TouchableOpacity
        onPress={() => navigation.navigate('ChangePassword')}
        style={styles.buttonContainerStyle}>
        <Text
          style={{
            flex: 1,
            fontSize: fontSizes.f14,
            fontFamily: fonts.SFProTextBold,
            color: 'black'
          }}>
          Change Password
        </Text>
        <CTIcon source={rightArrowIcon} iconStyle={{ width: 12, height: 12 }} />
      </TouchableOpacity>
      <View style={styles._horizontalLine} />
      <TouchableOpacity
        onPress={() => navigation.navigate('ChangePassword')}
        style={styles.buttonContainerStyle}>
        <Text
          style={{
            flex: 1,
            fontSize: fontSizes.f14,
            fontFamily: fonts.SFProTextBold,
            color: 'black'
          }}>
          Notification Settings
        </Text>
        <CTIcon source={rightArrowIcon} iconStyle={{ width: 12, height: 12 }} />
      </TouchableOpacity>
      <View style={styles._horizontalLine} />
      <TouchableOpacity
        onPress={() => setIsLogOut(true)}
        style={styles.buttonContainerStyle}>
        <Text
          style={{
            flex: 1,
            fontSize: fontSizes.f14,
            fontFamily: fonts.SFProTextBold,
            color: 'black'
          }}>
          Logout
        </Text>
        <CTIcon source={rightArrowIcon} iconStyle={{ width: 12, height: 12 }} />
      </TouchableOpacity>
      {/* <View style={{ height: 80 }} />
      <CTButton
        title="Logout"
        fullPress={() => setIsLogOut(true)}
        contentContainerStyle={{
          backgroundColor: colors.white,
          borderWidth: 2,
          borderColor: colors.red,
        }}
        titleStyle={{ color: colors.red }}
      /> */}
      <CTConfirmationBox
        isVisible={isLogOut}
        title="Logout"
        descriptionComponent={
          <Text
            style={{
              fontFamily: fonts.SFProTextRegular,
              color: colors.darkBlack,
              fontSize: fontSizes.f14,
            }}>
            Are you sure you want to logout?
          </Text>
        }
        dismissButtonTitle="No"
        confirmButtonTitle="Yes"
        onDismiss={setIsLogOut}
        onConfirm={onLogout}
      />
    </View>
  );
});
const styles = StyleSheet.create({
  buttonContainerStyle: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
  },
  _horizontalLine: {
    alignSelf: 'center',
    width: '90%',
    height: 1,
    backgroundColor: '#efefef',
  },
  settingTitle: {
    fontFamily: fonts.SFProTextSemiBold,
    fontSize: 12,
    color: colors.darkGrayDisable,
    marginVertical: 15
  }
});
