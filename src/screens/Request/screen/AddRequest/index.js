import React, { useState } from 'react';
import {
  View,
  KeyboardAvoidingView,
  SafeAreaView,
  Text,
  ScrollView,
  Platform,
  FlatList,
  StatusBar
} from 'react-native';
import styles from './styles';
import CTDateTimePicker from '../../../../components/CTDateTimePicker';
import { CTHeader } from '../../../../components/CTHeader';
import { CTIcon } from '../../../../components/CTIcon';
import { closeIcon } from '../../../../assets/icons';
import commonStyles from '../../../../constants/commonStyles';
import CTSelectBoxDropdown from '../../../../components/CTSelectBoxDropdown';
import { CTInputField } from '../../../../components/CTInputField';
import colors from '../../../../constants/colors';
import { CTSubHeadingLeft } from '../../../../components/CTHeading';
import { FUEL_ARRAY } from '../../../../constants';
import CTCheckBox from '../../../../components/CTCheckBox';
import { CTButton } from '../../../../components/Button';
import SimpleToast from '../../../../components/SimpleToast';

// @inject('authStore', 'loaderStore')
export default AddRequest = ({ navigation }) => {
  // const [dob, setDob] = useState(
  //   userInfo.dob.original === '' ? '' : new Date(userInfo.dob.original),
  // );

  const [dob, setDob] = useState('');

  const vehicleArray = [
    {
      business_name: '122',
    },
    {
      business_name: 'as',
    },
  ];

  const [driverContact, setDriverContact] = useState('');
  const [fuelQuality, setFuelQuality] = useState('');
  const [notes, setNotes] = useState('');

  const [vehicle, setVehicle] = useState({ business_name: '' });
  const [department, setDepartment] = useState({
    education_detail: '',
    education_id: '',
  });
  const departmentArray = [
    {
      education_detail: '011 Std.',
      education_id: 0,
    },

    {
      education_detail: '01 Std.',
      education_id: 1,
    },
    {
      education_detail: '02 Std.',
      education_id: 2,
    },
    {
      education_detail: '03 Std.',
      education_id: 3,
    },
    {
      education_detail: '04 Std.',
      education_id: 4,
    },
    {
      education_detail: '05 Std.',
      education_id: 5,
    },
    {
      education_detail: '06 Std.',
      education_id: 6,
    },
    {
      education_detail: '07 Std.',
      education_id: 7,
    },
    {
      education_detail: '08 Std.',
      education_id: 8,
    },
    {
      education_detail: '09 Std.',
      education_id: 9,
    },
    {
      education_detail: '10 Std.',
      education_id: 10,
    },
  ];

  const [fuelType, setFuelType] = useState('Desel');

  const genderRenderItem = ({ item, index }) => {
    const isSelected = fuelType == item;

    return (
      <View
        style={{
          flexDirection: 'row',
          marginLeft: index == 0 ? 10 : 0,
          marginRight: index + 1 == FUEL_ARRAY.length ? 10 : 0,
        }}>
        <CTCheckBox
          contentContainerStyle={styles.checkboxContainerStyle}
          checkIconVisible={false}
          selected={isSelected}
          title={item}
          onPress={() => setFuelType(item)}
        />
      </View>
    );
  };
  const _submit = async () => {
    if (!driverContact) {
      return SimpleToast('Enter Driver Contact No.');
    } else if (!fuelType) {
      return SimpleToast('Select Fuel Type');
    } else if (!dob) {
      return SimpleToast('Select Date Of Birth');
    } else if (!fuelQuality) {
      return SimpleToast('Select Fuel Quality');
    } else if (!maritalStatus) {
      return SimpleToast('Select Marital Status');
    } else if (occupation == OCCUPATION_ARRAY[0] && !department) {
      return SimpleToast('Select Department');
    } else if (occupation == OCCUPATION_ARRAY[2] && !vehicle) {
      return SimpleToast('Select Business Type');
    }
  };

  return (
    <View style={styles.containerStyle}>
      <SafeAreaView />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={StatusBar.currentHeight}
        style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="always">
          <CTHeader
            title="Add Request"
            headerTitleStyle={{ textAlign: 'left' }}
            isLeftBlank
            headerContainerStyle={{ marginLeft: 20 }}
            rightComp={
              <CTIcon
                source={closeIcon}
                onPress={() => navigation.goBack()}
                iconStyle={{ width: 24, height: 24 }}
              />
            }
          />
          <View style={styles.itemContainerStyle}>
            <CTDateTimePicker
              title="Date of Birth"
              selectedDate={dob}
              getSelectedDate={setDob}
              contentContainerStyle={styles.dateContentContainerStyle}
              titleContainerStyle={styles.dateTitleContainerStyle}
            />
          </View>
          <View style={styles.itemContainerStyle}>
            <CTSelectBoxDropdown
              style={commonStyles.dropdownStyle}
              selectedItem={department.education_detail}
              data={departmentArray}
              getSelectedItem={data => {
                setDepartment(data);
              }}
              itemTitleKey="education_detail"
              placeholder="Select Department"
              title="Department"
            />
          </View>
          <View style={styles.itemContainerStyle}>
            <CTSelectBoxDropdown
              style={commonStyles.dropdownStyle}
              selectedItem={vehicle.business_name}
              data={vehicleArray}
              getSelectedItem={setVehicle}
              itemTitleKey="business_name"
              placeholder="Select Vehicle"
              title="Vehicle"
              titleStyle={styles.titleStyle}
            />
          </View>
          <View style={styles.itemContainerStyle}>
            <CTInputField
              placeholder="Driver Contact No."
              title="Driver Contact No."
              placeholderTextColor={colors.gray}
              value={driverContact}
              onChangeText={setDriverContact}
            />
          </View>
          <View style={styles.itemContainerStyle}>
            <Text style={styles.txtCommon}>
              The driver will receive an SMS of fuel request. The driver needs
              to show this SMS at the Fuel Desk.
            </Text>
          </View>
          <View style={styles.inputItemSeparatorStyle} />

          <CTSubHeadingLeft
            title="Fuel Type"
            containerStyle={styles.subHeadingContainerStyle}
          />
          <FlatList
            keyboardShouldPersistTaps="always"
            bounces={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => String(index)}
            data={FUEL_ARRAY}
            renderItem={genderRenderItem}
          />

          <View style={styles.itemContainerStyle}>
            <CTInputField
              placeholder="Fuel Quantity"
              title="Fuel Quantity"
              placeholderTextColor={colors.gray}
              value={fuelQuality}
              onChangeText={setFuelQuality}
            />
          </View>
          <View style={styles.itemContainerStyle}>
            <CTInputField
              placeholder="Notes"
              title="Notes"
              placeholderTextColor={colors.gray}
              value={notes}
              onChangeText={setNotes}
              multiline={true}
            />
          </View>
          <View style={[styles.itemContainerStyle, { paddingVertical: 20 }]}>
            <CTButton fullPress={_submit} title="Submit Details" />
            <SafeAreaView />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};
