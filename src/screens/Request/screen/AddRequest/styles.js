import {StyleSheet} from 'react-native';
import colors from '../../../../constants/colors';
import {fonts} from '../../../../constants/fonts';
import fontSizes from '../../../../constants/fontSizes';

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  itemContainerStyle: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  ctheadingLeftContainerStyle: {
    marginLeft: 20,
    marginBottom: 8,
  },
  subHeadingContainerStyle: {
    marginLeft: 20,
  },
  dateContentContainerStyle: {
    paddingHorizontal: 0,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderRadius: 0,
  },
  dateTitleContainerStyle: {
    paddingHorizontal: 0,
    left: 0,
    color: colors.darkGrayDisable,
  },
  itemSeparatorStyle: {
    height: 20,
  },
  inputItemSeparatorStyle: {
    height: 27,
  },
  checkboxContainerStyle: {
    marginHorizontal: 5,
  },
  titleStyle: {
    color: colors.black,
    fontSize: fontSizes.f20,
    fontFamily: fonts.SFProTextBold,
  },
  txtCommon: {
    color: colors.black,
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextRegular,
  },
});
export default styles;
