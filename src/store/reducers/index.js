// import {createStore, combineReducers} from 'redux';
// import userReducer from './user.reducer';

// const rootReducer = combineReducers({userReducer: userReducer});

// const configureStore = () => {
//   return createStore(rootReducer);
// };

// export default configureStore;

import {combineReducers} from 'redux';
import userReducer from './user.reducer';

const appReducer = combineReducers({
  userReducer: userReducer,
});

export default rootReducer = (state, action) => {
  return appReducer(state, action);
};
