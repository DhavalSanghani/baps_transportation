import {SHOW_LOADER, HIDE_LOADER} from '../types/index';

const initialState = {
  loader: false,
  sabhaInfo: [],
};

export default function common(state = initialState, data) {
  switch (data.type) {
    case SHOW_LOADER:
      return {...state, loader: true};
    case HIDE_LOADER:
      return {...state, loader: false};
    // case SET_SABHA_INFO:
    //   return { ...state, sabhaInfo: data.payload };
    // case SET_HOME_SCREEN_IMAGES:
    //   return { ...state, homeScreenImages: data.payload };
    default:
      return {...state};
  }

  // return state;
}
