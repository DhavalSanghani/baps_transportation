// import { CLEAR_USER, SET_USER } from "../types";

// const intialState = {
//     user: null
// }

// function userReducer(state = intialState, action) {
//     let { type, payload } = action;
//     switch (type) {
//         case SET_USER: {
//             console.log("pl-->", payload);
//             return Object.assign({}, { ...state }, { user: payload.user });
//         }
//         case CLEAR_USER: {
//             return { user: null }
//         }
//         default:
//             return state;
//     }
// }

// export default userReducer;

import {SET_USER} from '../types';

const initialState = {
  user: '',
};
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      console.log('pl-->', action);
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
};
export default userReducer;
