// import { createStore } from "redux";
// import rootReducer from "./reducers";

// const configureStore = () => {
//     return createStore(rootReducer);
// }

// export default configureStore;

// // const store = createStore(rootReducer);

// // export default store;

// import {combineReducers} from 'redux';
// import UserReducer from '../store/reducers/user.reducer';

// const appReducer = combineReducers({userReducer: UserReducer});

// export default rootReducer = (state, action) => {
//   return appReducer(state, action);
// };

// export default configureStore;

import {createStore, combineReducers} from 'redux';
import UserReducer from '../store/reducers/user.reducer';
const rootReducer = combineReducers({userReducer: UserReducer});
const configureStore = () => {
  return createStore(rootReducer);
};
export default configureStore;
