export const SET_USER = 'SET_USER';
export const CLEAR_USER = 'CLEAR_USER';
export const SHOW_LOADER = 'SHOW APP LOADER';
export const HIDE_LOADER = 'HIDE APP LOADER';
