import React from 'react';
import {
  TouchableOpacity,
  Text,
  Image,
  View,
  TextInput,
  Animated,
  Platform,
  StyleSheet,
} from 'react-native';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import {useRef} from 'react';
import {useEffect} from 'react';
import {useState} from 'react';
import fontSizes from '../constants/fontSizes';

export function CTInputField({
  title,
  titleStyle,
  containerStyle,
  inputContainerStyle,
  inputStyle,
  leftSource,
  leftIconStyle,
  leftIconContainerStyle,
  leftOnPress,
  rightSource,
  rightIconStyle,
  rightIconContainerStyle,
  rightOnPress,
  fullTouchOnPress,
  isError,
  errorMSG,
  errorMSGStyle,
  height,
  secureTextEntry,
  animated = true,
  placeholder,
  onChangeText,
  placeholderTextColor,
  keyboardType,
  style,
  multiline,
  ...other
}) {
  containerStyle = Array.isArray(containerStyle)
    ? style?.containerStyle
      ? (containerStyle = [...containerStyle, style?.containerStyle])
      : (containerStyle = containerStyle)
    : [style?.containerStyle, containerStyle];
  titleStyle = Array.isArray(titleStyle)
    ? style?.titleStyle
      ? (titleStyle = [...titleStyle, style?.titleStyle])
      : (titleStyle = titleStyle)
    : [style?.titleStyle, titleStyle];
  inputContainerStyle = Array.isArray(inputContainerStyle)
    ? style?.inputContainerStyle
      ? (inputContainerStyle = [
          ...inputContainerStyle,
          style?.inputContainerStyle,
        ])
      : (inputContainerStyle = inputContainerStyle)
    : [style?.inputContainerStyle, inputContainerStyle];
  leftIconContainerStyle = Array.isArray(leftIconContainerStyle)
    ? style?.leftIconContainerStyle
      ? (leftIconContainerStyle = [
          ...leftIconContainerStyle,
          style?.leftIconContainerStyle,
        ])
      : (leftIconContainerStyle = leftIconContainerStyle)
    : [style?.leftIconContainerStyle, leftIconContainerStyle];
  leftIconStyle = Array.isArray(leftIconStyle)
    ? style?.leftIconStyle
      ? (leftIconStyle = [...leftIconStyle, style?.leftIconStyle])
      : (leftIconStyle = leftIconStyle)
    : [style?.leftIconStyle, leftIconStyle];
  inputStyle = Array.isArray(inputStyle)
    ? style?.inputStyle
      ? (inputStyle = [...inputStyle, style?.inputStyle])
      : (inputStyle = inputStyle)
    : [style?.inputStyle, inputStyle];
  rightIconContainerStyle = Array.isArray(rightIconContainerStyle)
    ? style?.rightIconContainerStyle
      ? (rightIconContainerStyle = [
          ...rightIconContainerStyle,
          style?.rightIconContainerStyle,
        ])
      : (rightIconContainerStyle = rightIconContainerStyle)
    : [style?.rightIconContainerStyle, rightIconContainerStyle];
  rightIconStyle = Array.isArray(rightIconStyle)
    ? style?.rightIconStyle
      ? (rightIconStyle = [...rightIconStyle, style?.rightIconStyle])
      : (rightIconStyle = rightIconStyle)
    : [style?.rightIconStyle, rightIconStyle];

  errorMSGStyle = Array.isArray(errorMSGStyle)
    ? style?.errorMSGStyle
      ? (errorMSGStyle = [...errorMSGStyle, style?.errorMSGStyle])
      : (errorMSGStyle = errorMSGStyle)
    : [style?.errorMSGStyle, errorMSGStyle];

  const inAnimValue = 16;
  const outAnimValue = Platform.OS == 'ios' ? -10 : -8;
  const duration = 150;
  const floatAnimation = useRef(new Animated.Value(0)).current;
  const textInputRef = useRef(null);
  const [myFontFamily, setMyFontFamily] = useState(fonts.SFProTextBold);
  const [myColor, setMyColor] = useState(colors.black);
  const [isFocused, setIsFocused] = useState(false);
  useEffect(() => {
    if (other.value === '' && !isFocused) {
      initAnimation(inAnimValue);
      setMyFontFamily(fonts.SFProTextBold);
      setMyColor(colors.black);
    } else {
      initAnimation(outAnimValue);
      setMyFontFamily(fonts.SFProTextMedium);
      setMyColor(colors.darkGrayDisable);
    }
  }, [other.value]);

  const initAnimation = animationValue => {
    Animated.timing(floatAnimation, {
      toValue: animationValue,
      duration: duration,
      useNativeDriver: false,
    }).start();
  };

  const inAnimation = () => {
    if (other.value === '') {
      Animated.timing(floatAnimation, {
        toValue: inAnimValue,
        duration: duration,
        useNativeDriver: false,
      }).start(data => {
        if (data.finished) {
          setMyFontFamily(fonts.SFProTextBold);
          setMyColor(colors.black);
        }
      });
    }
  };

  const outAnimation = () => {
    Animated.timing(floatAnimation, {
      toValue: outAnimValue,
      duration: duration,
      useNativeDriver: false,
    }).start(data => {
      if (data.finished) {
        setMyFontFamily(fonts.SFProTextMedium);
        setMyColor(colors.lightGray);
      }
    });
  };

  const __inputFocus = () => {
    setIsFocused(true);
    outAnimation();
  };
  const __inputBlur = () => {
    setIsFocused(false);
    inAnimation();
  };
  const TitleComp = ({title, titleStyle}) => {
    return (
      <Animated.Text
        onPress={() => textInputRef.current.focus()}
        style={[
          {
            position: 'absolute',
            backgroundColor: colors.white,
            left: 0,
            top: floatAnimation,
            zIndex: 10,
            fontSize: fontSizes.f11,
            transform: [
              {
                scale: floatAnimation.interpolate({
                  inputRange: [outAnimValue, inAnimValue],
                  outputRange: [1, 1],
                }),
              },
            ],
            color: myColor,
            fontFamily: myFontFamily,
          },
          titleStyle,
        ]}>
        {title}
      </Animated.Text>
    );
  };
  return (
    <View
      style={[
        {
          borderColor: colors.gray,
          borderBottomWidth: 0.6,
        },
        containerStyle,
      ]}>
      {title && animated ? (
        <TitleComp title={title} titleStyle={titleStyle} />
      ) : (
        title && (
          <Text
            style={[
              {
                fontSize: fontSizes.f12,
                color: colors.lightGray,
                fontFamily: fonts.SFProTextRegular,
                position: 'absolute',
                backgroundColor: colors.white,
                left: 15,
                top: -8,
                zIndex: 10,
              },
              titleStyle,
            ]}>
            {title}
          </Text>
        )
      )}

      <View
        style={[
          {
            flexDirection: 'row',
            overflow: 'hidden',
            borderWidth: 1,
            borderRadius: 10,
            height: height ? (height == 'undefined' ? undefined : height) : 48,
            borderColor: colors.gray,
            alignItems: 'center',
            paddingHorizontal: 15,
            ...styles.inputContainerStyle,
          },
          inputContainerStyle,
        ]}>
        {leftSource && (
          <TouchableOpacity
            onPress={leftOnPress ? leftOnPress : null}
            activeOpacity={leftOnPress ? 0 : 1}
            style={[
              {
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: colors.transparent,
                width: 50,
              },
              leftIconContainerStyle,
            ]}>
            <Image
              source={leftSource}
              style={[
                {
                  width: 20,
                  height: 20,
                  tintColor: colors.lightGray,
                  resizeMode: 'contain',
                },
                leftIconStyle,
              ]}
            />
          </TouchableOpacity>
        )}

        <TextInput
          ref={textInputRef}
          onFocus={__inputFocus}
          onBlur={__inputBlur}
          style={[
            {
              flex: 1,
              // height: height
              //   ? height == 'undefined'
              //     ? undefined
              //     : height
              //   : 48,

              backgroundColor: colors.white,
              color: colors.black,
              fontSize: fontSizes.f14,
              paddingVertical: 0,
              paddingHorizontal: 0,
              fontFamily: fonts.SFProTextBold,
            },
            inputStyle,
          ]}
          onChangeText={onChangeText}
          placeholderTextColor={placeholderTextColor || colors.gray}
          secureTextEntry={secureTextEntry}
          {...other}
          keyboardType={
            keyboardType
              ? keyboardType
              : Platform.OS == 'ios'
              ? 'ascii-capable'
              : 'default'
          }
          multiline={multiline}
          returnKeyType={!multiline ? 'done' : undefined}
          placeholder={title ? '' : placeholder}
        />
        {rightSource && (
          <TouchableOpacity
            onPress={rightOnPress ? rightOnPress : null}
            activeOpacity={rightOnPress ? 0 : 1}
            style={[
              {
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: colors.white,
                // width: 50,
              },
              rightIconContainerStyle,
            ]}>
            <Image
              source={rightSource}
              style={[
                {
                  width: 20,
                  height: 20,
                  // tintColor: colors.black,
                  resizeMode: 'contain',
                },
                rightIconStyle,
              ]}
            />
          </TouchableOpacity>
        )}

        {fullTouchOnPress && (
          <TouchableOpacity
            onPress={fullTouchOnPress}
            style={{height: '100%', width: '100%', position: 'absolute'}}
          />
        )}
      </View>
      {isError && errorMSG && (
        <Text style={{fontSize: fontSizes.f10, color: 'red', ...errorMSGStyle}}>
          {errorMSG}
        </Text>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  inputContainerStyle: {
    paddingHorizontal: 0,
    borderWidth: 0,
    borderBottomWidth: 0,
    borderRadius: 0,
  },
  titleStyle: {left: 0},
});
CTInputField.propTypes = {};
