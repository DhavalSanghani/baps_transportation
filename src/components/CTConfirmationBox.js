import React from 'react';
import {View, Text, Modal, StyleSheet, Image} from 'react-native';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSizes from '../constants/fontSizes';
import {CTButton} from './Button';
export default function CTConfirmationBox({
  isVisible,
  title,
  description,
  onConfirm,
  onDismiss,
  confirmButtonTitle,
  confirmButtonTitleStyle,
  dismissButtonTitle,
  dismissButtonTitleStyle,
  descriptionTextStyle,
  modalStyle,
  containerStyle,
  dismissButtonStyle,
  confirmButtonStyle,
  iconContainerStyle,
  iconSource,
  iconStyle,
  descriptionComponent,
}) {
  return (
    <Modal
      transparent
      visible={isVisible}
      supportedOrientations={['landscape', 'portrait']}>
      <View style={[styles.containerStyle, containerStyle]}>
        <View style={[styles.modalStyle, modalStyle]}>
          {iconSource && (
            <View
              style={[
                {
                  alignItems: 'center',
                },
                iconContainerStyle,
              ]}>
              <Image
                source={iconSource}
                style={[{width: 50, height: 50}, iconStyle]}
              />
            </View>
          )}
          {title && (
            <View>
              <Text style={styles.titleStyle}>{title}</Text>
              <View
                style={{
                  marginVertical: 10,
                  height: 1,
                  width: '100%',
                  backgroundColor: colors.grayDisable,
                  borderRadius: 5,
                }}
              />
            </View>
          )}
          {description ? (
            <Text style={[styles.descriptionTextStyle, descriptionTextStyle]}>
              {description}
            </Text>
          ) : (
            <View>{descriptionComponent}</View>
          )}
          <View style={styles.buttonContainerStyle}>
            <CTButton
              contentContainerStyle={[
                {
                  width: '48%',
                  backgroundColor: colors.grayDisable,
                },
                dismissButtonStyle,
              ]}
              titleStyle={[
                {color: colors.darkGrayDisable},
                dismissButtonTitleStyle,
              ]}
              leftComp={<View />}
              rightComp={<View />}
              title={dismissButtonTitle}
              fullPress={() => onDismiss && onDismiss(false)}
            />
            <CTButton
              contentContainerStyle={[
                {
                  width: '48%',
                  backgroundColor: colors.red,
                },
                confirmButtonStyle,
              ]}
              leftComp={<View />}
              rightComp={<View />}
              title={confirmButtonTitle}
              titleStyle={confirmButtonTitleStyle}
              fullPress={() => onConfirm && onConfirm(false)}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
}
const styles = StyleSheet.create({
  buttonContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  titleStyle: {
    fontSize: fontSizes.f18,
    color: colors.black,
    fontFamily: fonts.SFProTextBold,
    marginBottom: 10,
  },
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  descriptionTextStyle: {
    fontSize: fontSizes.f14,
    color: colors.black,
    marginVertical: 10,
  },
  modalStyle: {
    padding: 25,
    marginHorizontal: 15,
    backgroundColor: 'white',
    borderRadius: 25,
  },
});
