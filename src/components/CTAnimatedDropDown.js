import {View} from 'react-native';
import React from 'react';
export const CTAnimatedDropDown = ({title, children, isOpen}) => {
  return (
    <>
      <View>{title}</View>
      {isOpen && <View style={[!isOpen && {height: 0}]}>{children}</View>}
    </>
  );
};
