import propTypes from 'prop-types';
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  ViewPropTypes,
  ActivityIndicator,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { deleteIcon, cameraIcon } from '../assets/icons/index';
import { userPlaceholder } from '../assets/image/index';

import colors from '../constants/colors';

export default function CTAvatar({
  onPressEdit,
  onPress,
  onPressDelete,
  imagePlaceholderSource,
  source,
  size,
  borderWidth,
  imagePlaceholderStyle,
  imageStyle,
  style,
  editIconStyle,
  editIconContainerStyle,
  deleteIconStyle,
  deleteIconContainerStyle,
  contentContainerStyle,
  resizeMode,
  isLoader,
  loaderBorderVariation = 20,
}) {
  const [imagePlaceholder, setImagePlaceholder] = React.useState(
    imagePlaceholderSource || userPlaceholder,
  );
  size = size == null || size == undefined ? 100 : size;
  borderWidth =
    borderWidth !== null || borderWidth !== undefined ? borderWidth : 2;

  contentContainerStyle = Array.isArray(contentContainerStyle)
    ? style?.contentContainerStyle
      ? (contentContainerStyle = [
        ...contentContainerStyle,
        style?.contentContainerStyle,
      ])
      : (contentContainerStyle = contentContainerStyle)
    : [style?.contentContainerStyle, contentContainerStyle];
  imagePlaceholderStyle = Array.isArray(imagePlaceholderStyle)
    ? style?.imagePlaceholderStyle
      ? (imagePlaceholderStyle = [
        ...imagePlaceholderStyle,
        style?.imagePlaceholderStyle,
      ])
      : (imagePlaceholderStyle = imagePlaceholderStyle)
    : [style?.imagePlaceholderStyle, imagePlaceholderStyle];
  imageStyle = Array.isArray(imageStyle)
    ? style?.imageStyle
      ? (imageStyle = [...imageStyle, style?.imageStyle])
      : (imageStyle = imageStyle)
    : [style?.imageStyle, imageStyle];
  editIconContainerStyle = Array.isArray(editIconContainerStyle)
    ? style?.editIconContainerStyle
      ? (editIconContainerStyle = [
        ...editIconContainerStyle,
        style?.editIconContainerStyle,
      ])
      : (editIconContainerStyle = editIconContainerStyle)
    : [style?.editIconContainerStyle, editIconContainerStyle];
  deleteIconContainerStyle = Array.isArray(deleteIconContainerStyle)
    ? style?.deleteIconContainerStyle
      ? (deleteIconContainerStyle = [
        ...deleteIconContainerStyle,
        style?.deleteIconContainerStyle,
      ])
      : (deleteIconContainerStyle = deleteIconContainerStyle)
    : [style?.deleteIconContainerStyle, deleteIconContainerStyle];
  deleteIconStyle = Array.isArray(deleteIconStyle)
    ? style?.deleteIconStyle
      ? (deleteIconStyle = [...deleteIconStyle, style?.deleteIconStyle])
      : (deleteIconStyle = deleteIconStyle)
    : [style?.deleteIconStyle, deleteIconStyle];
  editIconStyle = Array.isArray(editIconStyle)
    ? style?.editIconStyle
      ? (editIconStyle = [...editIconStyle, style?.editIconStyle])
      : (editIconStyle = editIconStyle)
    : [style?.editIconStyle, editIconStyle];
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onPress}
      style={[
        {
          width: size,
          height: size,
          borderWidth: borderWidth,
        },
        styles.contentContainerStyle,
        ...contentContainerStyle,
      ]}>
      <ImageBackground
        source={imagePlaceholder}
        style={[styles.imagePlaceholderStyle, ...imagePlaceholderStyle]}
        resizeMode="contain">
        <FastImage
          style={[styles.imageStyle, ...imageStyle]}
          source={source}
          resizeMode={resizeMode || 'contain'}
          onLoad={() => {
            setImagePlaceholder(null);
          }}
        />
        {isLoader && (
          <View
            style={{
              position: 'absolute',
              zIndex: 10000,
              left: 1,
              top: 0,
              backgroundColor: 'rgba(0,0,0,.5)',
              justifyContent: 'center',
              alignItems: 'center',
              width: size - loaderBorderVariation,
              height: size - loaderBorderVariation,
              borderRadius: size,
            }}>
            <ActivityIndicator size={'small'} color={colors.white} />
          </View>
        )}
      </ImageBackground>
      {onPressEdit && (
        <TouchableOpacity
          onPress={onPressEdit}
          activeOpacity={1}
          style={[styles.editIconContainerStyle, ...editIconContainerStyle]}>
          <FastImage
            source={cameraIcon}
            style={[styles.editIconStyle, ...editIconStyle]}
            tintColor={colors.black}
          />
        </TouchableOpacity>
      )}
      {onPressDelete && (
        <TouchableOpacity
          onPress={onPressDelete}
          activeOpacity={1}
          style={[
            styles.deleteIconContainerStyle,
            ...deleteIconContainerStyle,
          ]}>
          <FastImage
            source={deleteIcon}
            tintColor={colors.white}
            style={[styles.deleteIconStyle, ...deleteIconStyle]}
          />
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  contentContainerStyle: {
    borderRadius: 1000,
  },
  imagePlaceholderStyle: {
    flex: 1,
  },
  imageStyle: { flex: 1, borderRadius: 1000 },
  editIconContainerStyle: {
    position: 'absolute',
    width: 30,
    height: 30,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    bottom: 0,
    right: -5,
  },
  deleteIconContainerStyle: {
    position: 'absolute',
    width: 30,
    height: 30,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    bottom: 5,
    left: 0,
  },
  deleteIconStyle: {
    width: 16,
    height: 16,
  },
  editIconStyle: {
    width: 16,
    height: 16,
  },
});
CTAvatar.propTypes = {
  onPressEdit: propTypes.func,
  onPress: propTypes.func,
  onPressDelete: propTypes.func,
  // imagePlaceholderSource,
  // source: propTypes.string,
  size: propTypes.number,
  borderWidth: propTypes.number,
  imagePlaceholderStyle: ViewPropTypes.style,
  imageStyle: propTypes.object,
  editIconStyle: propTypes.object,
  editIconContainerStyle: ViewPropTypes.style,
  deleteIconStyle: propTypes.object,
  deleteIconContainerStyle: ViewPropTypes.style,
  contentContainerStyle: ViewPropTypes.style,
  style: propTypes.shape({
    imagePlaceholderStyle: ViewPropTypes.style,
    imageStyle: propTypes.object,
    editIconStyle: propTypes.object,
    editIconContainerStyle: ViewPropTypes.style,
    deleteIconStyle: propTypes.object,
    deleteIconContainerStyle: ViewPropTypes.style,
    contentContainerStyle: ViewPropTypes.style,
  }),
};
