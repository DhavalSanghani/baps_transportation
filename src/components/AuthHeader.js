import React from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  Image,
  TouchableOpacity,
  Keyboard,
  Dimensions,
} from 'react-native';
import { templeLogoImage } from '../assets/image';
import colors from '../constants/colors';

const { width } = Dimensions.get('window');

export default function AuthHeader({ children }) {
  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle={'light-content'} />
      <TouchableOpacity
        activeOpacity={1}
        style={styles.container}
        onPress={() => Keyboard.dismiss()}>
        <View style={styles.iconContainerStyle}>
          <Image source={templeLogoImage} style={styles.iconStyle} />
        </View>
        {children}
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.white },
  iconContainerStyle: {
    flex: 0.5,
    backgroundColor: colors.primary,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginContainerStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  iconStyle: {
    width: width / 2,
    height: width / 2 * 1.2,
    resizeMode: 'contain',
  },
});
