import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Modal from 'react-native-modal';
import { closeIcon } from '../assets/icons';
import colors from '../constants/colors';
import { fonts } from '../constants/fonts';
import fontSizes from '../constants/fontSizes';
import { CTSubHeadingLeft } from './CTHeading';
import { CTIcon } from './CTIcon';
export default function CTActionSheet({
  isVisible,
  data,
  itemContainerStyle,
  itemStyle,
  itemTextStyle,
  cancelStyle,
  onCancelPress,
  onBackdropPress,
  containerStyle,
  selectedOption,
  children
}) {
  return (
    <Modal
      isVisible={isVisible}
      style={{ margin: 0, justifyContent: 'flex-end' }}
      useNativeDriver
      swipeDirection={['down']}
      onBackdropPress={() => onBackdropPress(false)}
      onBackButtonPress={() => onBackdropPress(false)}>
      <View style={[styles.container, containerStyle]}>
        <View style={{ flex: 1 }} />
        {onCancelPress && (
          <>
            <View style={{ height: 10 }} />
            <TouchableOpacity
              onPress={() => onCancelPress(false)}
              style={[styles.cancelContainer, cancelStyle]}>
              <CTIcon
                source={closeIcon}
                disabled={true}
                iconStyle={{ width: 24, height: 24 }}
              />
            </TouchableOpacity>
          </>
        )}
        <View style={[styles.itemContainer, itemContainerStyle]}>
          {children}
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    overflow: 'hidden',
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingHorizontal: 32,
    paddingVertical: 40,
    backgroundColor: colors.white,
  },
  item: {
    alignItems: 'center',
    backgroundColor: colors.white,
    flexDirection: 'row',
  },
  horizontalLine: {
    borderTopColor: '#efefef',
    borderTopWidth: 0.5,
  },
  cancleText: {
    color: 'red',
  },
  cancelContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginBottom: 15,
    borderRadius: 25,
    alignSelf: 'center',
  },
  container: {
    margin: 0,
  },
  title: {
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
