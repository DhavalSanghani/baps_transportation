import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ViewPropTypes,
  StatusBar,
} from 'react-native';
import {leftArrowIcon} from '../assets/icons';
import PropTypes from 'prop-types';
import colors from '../constants/colors';
import fontSizes from '../constants/fontSizes';
import {fonts} from '../constants/fonts';

export function CTHeader({
  title,
  headerTitleStyle,
  subtitle,
  headerSubtitleStyle,
  source,
  onBackPress,
  backButtonStyle,
  backIconStyle,
  headerAlign,
  rightComp,
  rightCompContainerStyle,
  leftComp,
  headerColor,
  leftCompContainerStyle,
  headerContainerStyle,
  statusbarStyle,
  statusbarColor,
  isLeftBlank,
  isRightFull,
  useBackDrop,
  noStatusBar,
  style,
}) {
  headerContainerStyle = Array.isArray(headerContainerStyle)
    ? style?.headerContainerStyle
      ? (headerContainerStyle = [
          ...headerContainerStyle,
          style?.headerContainerStyle,
        ])
      : (headerContainerStyle = headerContainerStyle)
    : [style?.headerContainerStyle, headerContainerStyle];
  backButtonStyle = Array.isArray(backButtonStyle)
    ? style?.backButtonStyle
      ? (backButtonStyle = [...backButtonStyle, style?.backButtonStyle])
      : (backButtonStyle = backButtonStyle)
    : [style?.backButtonStyle, backButtonStyle];
  backIconStyle = Array.isArray(backIconStyle)
    ? style?.backIconStyle
      ? (backIconStyle = [...backIconStyle, style?.backIconStyle])
      : (backIconStyle = backIconStyle)
    : [style?.backIconStyle, backIconStyle];
  leftCompContainerStyle = Array.isArray(leftCompContainerStyle)
    ? style?.leftCompContainerStyle
      ? (leftCompContainerStyle = [
          ...leftCompContainerStyle,
          style?.leftCompContainerStyle,
        ])
      : (leftCompContainerStyle = leftCompContainerStyle)
    : [style?.leftCompContainerStyle, leftCompContainerStyle];
  headerSubtitleStyle = Array.isArray(headerSubtitleStyle)
    ? style?.headerSubtitleStyle
      ? (headerSubtitleStyle = [
          ...headerSubtitleStyle,
          style?.headerSubtitleStyle,
        ])
      : (headerSubtitleStyle = headerSubtitleStyle)
    : [style?.headerSubtitleStyle, headerSubtitleStyle];
  headerTitleStyle = Array.isArray(headerTitleStyle)
    ? style?.headerTitleStyle
      ? (headerTitleStyle = [...headerTitleStyle, style?.headerTitleStyle])
      : (headerTitleStyle = headerTitleStyle)
    : [style?.headerTitleStyle, headerTitleStyle];
  rightCompContainerStyle = Array.isArray(rightCompContainerStyle)
    ? style?.rightCompContainerStyle
      ? (rightCompContainerStyle = [
          ...rightCompContainerStyle,
          style?.rightCompContainerStyle,
        ])
      : (rightCompContainerStyle = rightCompContainerStyle)
    : [style?.rightCompContainerStyle, rightCompContainerStyle];

  return (
    <View
      style={[
        styles.headerContainer,
        {backgroundColor: headerColor || colors.white},
        ...headerContainerStyle,
      ]}>
      {!noStatusBar && (
        <StatusBar
          barStyle={statusbarStyle || 'dark-content'}
          backgroundColor={statusbarColor || headerColor || 'white'}
        />
      )}

      {onBackPress && onBackPress !== null ? (
        <TouchableOpacity
          style={[styles.backButtonStyle, ...backButtonStyle]}
          onPress={onBackPress}>
          {useBackDrop ? (
            <View style={styles.backDropStyle}>
              <Image
                style={[
                  styles.backIconStyle,
                  {tintColor: colors.white},
                  ...backIconStyle,
                ]}
                source={source ? source : leftArrowIcon}
              />
            </View>
          ) : (
            <Image
              style={[
                styles.backIconStyle,
                {tintColor: colors.black},
                ...backIconStyle,
              ]}
              source={source ? source : leftArrowIcon}
            />
          )}
        </TouchableOpacity>
      ) : leftComp ? (
        <View
          style={[styles.leftCompContainerStyle, ...leftCompContainerStyle]}>
          {leftComp}
        </View>
      ) : isLeftBlank ? null : (
        <View style={{width: 60, height: '100%'}} />
      )}
      <View style={{flex: 1, height: '100%', justifyContent: 'center'}}>
        <Text
          numberOfLines={1}
          style={[
            styles.headerTitleStyle,
            {textAlign: headerAlign ? headerAlign : 'center'},
            ...headerTitleStyle,
          ]}>
          {title}
        </Text>
        {subtitle && (
          <Text
            style={[
              styles.headerSubtitleStyle,
              {
                textAlign: headerAlign ? headerAlign : 'center',
              },
              ...headerSubtitleStyle,
            ]}>
            {subtitle}
          </Text>
        )}
      </View>
      {!isRightFull ? (
        <View style={[styles.rightCompContainerStyle, rightCompContainerStyle]}>
          {rightComp}
        </View>
      ) : (
        rightComp
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  rightCompContainerStyle: {
    height: '100%',
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerSubtitleStyle: {
    fontSize: fontSizes.f12,
    width: '100%',
    color: '#000',
  },
  headerTitleStyle: {
    width: '100%',
    color: colors.black,
    fontSize: fontSizes.f20,
    fontFamily: fonts.SFProTextBold,
  },
  leftCompContainerStyle: {
    height: '100%',
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerContainer: {
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
  },
  backButtonStyle: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backDropStyle: {
    backgroundColor: 'rgba(0,0,0,.5)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    width: 35,
    height: 35,
  },
  backIconStyle: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});

CTHeader.propTypes = {
  title: PropTypes.string,
  headerTitleStyle: PropTypes.object,
  subtitle: PropTypes.string,
  headerSubtitleStyle: PropTypes.object,
  source: PropTypes.any,
  onBackPress: PropTypes.func,
  backButtonStyle: PropTypes.object,
  backIconStyle: PropTypes.object,
  headerAlign: PropTypes.oneOf(['auto', 'center', 'left', 'right', 'justify']),
  rightComp: PropTypes.object,
  rightCompContainerStyle: PropTypes.object,
  leftComp: PropTypes.object,
  leftCompContainerStyle: PropTypes.object,
  headerContainerStyle: PropTypes.object,
  headerColor: PropTypes.string,
  statusbarStyle: PropTypes.string,
  statusbarColor: PropTypes.string,
  style: PropTypes.shape({
    headerTitleStyle: PropTypes.object,
    headerSubtitleStyle: PropTypes.object,
    backButtonStyle: ViewPropTypes.style,
    backIconStyle: PropTypes.object,
    rightCompContainerStyle: PropTypes.object,
    leftCompContainerStyle: ViewPropTypes.style,
    headerContainerStyle: ViewPropTypes.style,
  }),
};
