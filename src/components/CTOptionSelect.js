import React from 'react';
import {TouchableOpacity, Text, Image, View} from 'react-native';
import PropTypes from 'prop-types';
import colors from '../constants/colors';
import {useState} from 'react';
import {checkIcon, whiteCheckIcon} from '../assets/icons';
import FastImage from 'react-native-fast-image';
import fontSizes from '../constants/fontSizes';

export function CTOptionSelect({
  title,
  titleStyle,
  containerStyle,
  contentContainerStyle,
  optionPosition,
  checkStyle,
  optionStyle,
  size,
  source,
  shape,
  status,
  isRadio,
  ...other
}) {
  size = size ? size : 20;
  return (
    <TouchableOpacity style={{...containerStyle}} {...other}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          ...contentContainerStyle,
        }}>
        {(optionPosition === undefined || optionPosition === 'left') && (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: size,
              height: size,
              borderWidth: 1,
              borderColor: colors.primary,

              borderRadius: shape === 'round' ? size : 0,
              ...optionStyle,
            }}>
            {!isRadio
              ? status && (
                  <FastImage
                    source={source ? source : whiteCheckIcon}
                    style={{
                      width: size - 10,
                      height: size - 10,
                      resizeMode: 'contain',
                      ...checkStyle,
                    }}
                    resizeMode="contain"
                    tintColor={colors.white}
                  />
                )
              : status && (
                  <View
                    style={{
                      width: size - 8,
                      height: size - 8,
                      backgroundColor: colors.primary,
                      borderRadius: shape === 'round' ? size : 0,
                      ...checkStyle,
                    }}
                  />
                )}
          </View>
        )}
        {title && (
          <Text
            style={{
              fontSize: fontSizes.f14,
              color: colors.gray,
              ...titleStyle,
            }}>
            {title}
          </Text>
        )}
        {optionPosition === 'right' && (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: size,
              height: size,
              borderWidth: 1,
              borderColor: colors.lightLightGray,
              // borderColor: '#000000',
              marginLeft: 10,
              borderRadius: shape === 'round' ? size : 0,
              ...optionStyle,
            }}>
            {!isRadio
              ? status && (
                  <Image
                    source={source ? source : checkIcon}
                    style={{
                      width: size - 10,
                      height: size - 10,
                      resizeMode: 'contain',
                      ...checkStyle,
                    }}
                  />
                )
              : status && (
                  <View
                    style={{
                      width: size - 8,
                      height: size - 8,
                      backgroundColor: colors.primary,
                      borderRadius: shape === 'round' ? size : 0,
                      ...checkStyle,
                    }}
                  />
                )}
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
}
CTOptionSelect.propTypes = {
  titleStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  optionPosition: PropTypes.oneOf(['left', 'right']),
  checkStyle: PropTypes.object,
  optionStyle: PropTypes.object,
  size: PropTypes.number,
  shape: PropTypes.oneOf(['round', 'square']),
  status: PropTypes.bool.isRequired,
  isRadio: PropTypes.bool,
};

export function CTOptionsGroup({
  data,
  selected,
  onChange,
  horizontal,
  optionContainerStyle,
  contentContainerStyle,
  subContainerStyle,
  seperatorComponent,
  numColumns,
  ...other
}) {
  const createGroup = () => {
    let dataGroup = [];
    let groupCount = 0;
    let group = [];
    data.map((item, index) => {
      if (groupCount === 0 && index !== 0) {
        dataGroup.push(group);
        group = [];
      }
      group.push(item);
      groupCount++;
      if (groupCount === numColumns) groupCount = 0;
      if (data.length - 1 === index) dataGroup.push(group);
    });
    return dataGroup;
  };

  const [groupData, setGroupData] = useState(data);

  const isArray = Array.isArray(selected);

  const isCheckboxSelected = value => {
    return selected.filter(item => item === value).length !== 0 ? true : false;
  };

  const onChangeCheckbox = value => {
    let selectedData = [...selected];
    if (selectedData.filter(item => item === value).length !== 0) {
      selectedData = selectedData.filter(item => item !== value);
    } else {
      selectedData.push(value);
    }
    onChange(selectedData);
  };
  const checkboxStatus = !other.isRadio && !isArray;
  const radioStatus = other.isRadio && isArray ? true : false;

  let loadComp = () =>
    (numColumns && numColumns === 1) || !numColumns ? (
      groupData.map((item, index) => {
        return (
          <View
            key={String(index)}
            style={{flexDirection: horizontal ? 'row' : 'column'}}>
            <View>
              <CTOptions
                status={
                  other.isRadio
                    ? item.value === selected
                    : isCheckboxSelected(item.value)
                }
                onPress={() =>
                  other.isRadio
                    ? onChange(item.value)
                    : onChangeCheckbox(item.value)
                }
                title={item.title}
                containerStyle={optionContainerStyle}
                {...other}
              />
            </View>
            {seperatorComponent && seperatorComponent}
          </View>
        );
      })
    ) : (
      <View>
        {createGroup() &&
          createGroup().map((subData, index1) => (
            <View key={String(index1)} style={{flexDirection: 'row'}}>
              {subData.map((item, index) => (
                <View
                  key={String(index)}
                  style={{
                    flexDirection: 'row',
                    ...subContainerStyle,
                  }}>
                  <CTOptions
                    status={
                      other.isRadio
                        ? item.value === selected
                        : isCheckboxSelected(item.value)
                    }
                    onPress={() =>
                      other.isRadio
                        ? onChange(item.value)
                        : onChangeCheckbox(item.value)
                    }
                    title={item.title}
                    containerStyle={optionContainerStyle}
                    {...other}
                  />
                  {seperatorComponent && seperatorComponent}
                </View>
              ))}
            </View>
          ))}
      </View>
    );

  return (
    <View
      style={{
        flexDirection: horizontal ? 'row' : 'column',
        ...contentContainerStyle,
      }}>
      {!other.isRadio ? (
        checkboxStatus ? (
          <Text style={{color: 'red'}}>
            For Option selected data must be an array
          </Text>
        ) : (
          loadComp()
        )
      ) : radioStatus ? (
        <Text style={{color: 'red'}}>
          For Option selected data must be an single value
        </Text>
      ) : (
        loadComp()
      )}
    </View>
  );
}
CTOptionsGroup.propTypes = {};
