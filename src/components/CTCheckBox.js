import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {checkIcon} from '../assets/icons';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSizes from '../constants/fontSizes';

export default function CTCheckBox({
  onPress,
  title,
  titleStyle,
  selected,
  checkIconStyle,
  contentContainerStyle,
  checkIconVisible,
}) {
  checkIconVisible = checkIconVisible == undefined ? true : checkIconVisible;
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={{
        backgroundColor: selected ? colors.aliceBlue : colors.grayDisable,
        ...styles.contentContainer,
        ...contentContainerStyle,
      }}>
      {selected && checkIconVisible && (
        <FastImage
          source={checkIcon}
          style={{
            ...styles.checkIconStyle,
            ...checkIconStyle,
          }}
          resizeMode="contain"
        />
      )}
      <Text
        style={{
          fontFamily: selected ? fonts.SFProTextBold : fonts.SFProTextMedium,
          color: selected ? colors.primary : colors.black,
          ...styles.titleStyle,
          ...titleStyle,
        }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  contentContainer: {
    marginHorizontal: 5,
    paddingHorizontal: 16,
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 100,
    paddingVertical: 10,
    flexShrink: 1,
  },
  titleStyle: {
    fontSize: fontSizes.f12,
    flexShrink: 1,
  },
  checkIconStyle: {
    width: 14,
    height: 14,
    marginRight: 10,
  },
});
