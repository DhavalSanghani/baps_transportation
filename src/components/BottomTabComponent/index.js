import React, { useRef } from 'react';
import styled from 'styled-components/native';
import { Transition, Transitioning } from 'react-native-reanimated';
import Ionicons from 'react-native-vector-icons/Ionicons';

const bgColors = {
  requests: '#EBF4FC',
  profile: '#EBF4FC',
  notification: '#EBF4FC',
};

const textColors = {
  requests: '#3990EE',
  profile: '#3990EE',
  notification: '#3990EE',
};

const Container = styled.TouchableWithoutFeedback``;

const Background = styled(Transitioning.View)`
  flex: auto;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  align-self: center;
  background: ${(props) =>
    props.focused ? bgColors[props.label] : 'transparent'};
  border-radius: 100px;
  margin-horizontal: 4px;
  margin-vertical: 10px;
  height: 40px;
`;
const Icon = styled.Image`
  height: 24px;
  width: 24px;
`;

const Label = styled.Text`
  color: ${(props) => textColors[props.label]};
  font-family: Quicksand-Bold;
  margin-left: 8px;
`;

function Tab({ label, accessibilityState, onPress }) {
  const focused = accessibilityState.selected;
  // const icon = !focused ? Images.icons[label] : Images.icons[`${label}Focused`];

  const transition = (
    <Transition.Sequence>
      <Transition.Out type="fade" durationMs={0} />
      <Transition.Change interpolation="easeInOut" durationMs={100} />
      <Transition.In type="fade" durationMs={10} />
    </Transition.Sequence>
  );

  const ref = useRef();

  return (
    <Container
      onPress={() => {
        ref.current.animateNextTransition();
        onPress();
      }}
    >
      <Background
        focused={focused}
        label={label}
        ref={ref}
        transition={transition}
      >
        {label == 'requests' && (
          <Ionicons
            name={'list-circle-outline'}
            size={24}
            color={focused ? '#3990EE' : 'black'}
          />
        )}
        {label == 'profile' && (
          <Ionicons
            name={'person-circle-outline'}
            size={24}
            color={focused ? '#3990EE' : 'black'}
          />
        )}
        {label == 'notification' &&
          <Ionicons
            name={'notifications-outline'}
            size={24}
            color={focused ? '#3990EE' : 'black'}
          />
        }
        {focused && (
          <Label label={label}>
            {label.charAt(0).toUpperCase() + label.slice(1)}
          </Label>
        )}
      </Background>
    </Container>
  );
}

export default Tab;
