import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import colors from '../constants/colors';

export default function SettingProvider() {
  return <StatusBar barStyle="dark-content" backgroundColor={colors.white} />;
}
