import React from 'react';
import {View, Text} from 'react-native';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSizes from '../constants/fontSizes';

export function CTHeadingBottom({
  title,
  titleStyle,
  smallTitle,
  dashColor,
  containerStyle,
}) {
  return (
    <View style={{marginTop: 24, ...containerStyle}}>
      <Text
        style={{
          fontSize: fontSizes.f13,
          fontFamily: fonts.SFProTextBold,
          ...titleStyle,
        }}>
        {title}
        {smallTitle && (
          <Text
            style={{
              fontSize: fontSizes.f10,
              fontFamily: fonts.SFProTextRegular,
            }}>
            {smallTitle}
          </Text>
        )}
      </Text>
      <View
        style={{
          width: 15,
          height: 3,
          backgroundColor: dashColor || colors.gray,
          marginTop: 8,
          borderRadius: 10,
        }}
      />
    </View>
  );
}
export function CTHeadingLeft({
  title,
  titleStyle,
  containerStyle,
  leftComponent,
  showDash = true,
}) {
  return (
    <View
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center',
          marginVertical: 10,
        },
        containerStyle,
      ]}>
      {showDash && (
        <View
          style={[
            {
              backgroundColor: colors.primary,
              height: 4,
              width: 15,
              marginRight: 9,
              borderRadius: 10,
            },
          ]}
        />
      )}
      <Text
        style={{
          fontSize: fontSizes.f12,
          fontFamily: fonts.SFProTextBold,
          ...titleStyle,
        }}>
        {title}
      </Text>
      {leftComponent && leftComponent()}
    </View>
  );
}

export function CTSubHeadingLeft({containerStyle, title, titleStyle}) {
  return (
    <View style={[{marginBottom: 14, marginLeft: 20}, containerStyle]}>
      <Text
        style={[
          {
            fontSize: fontSizes.f11,
            color: colors.darkGrayDisable,
            fontFamily: fonts.SFProTextMedium,
          },
          titleStyle,
        ]}>
        {title}
      </Text>
    </View>
  );
}
