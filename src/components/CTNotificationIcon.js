import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {notificationIcon} from '../assets/icons';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSizes from '../constants/fontSizes';

export default function CTNotificationIcon({
  unreadNotificationCounter,
  onPress,
  notificationContainerStyle,
  notificationCounterStyle,
  notificationCounterTextStyle,
}) {
  return (
    <View
      style={[styles.notificationContainerStyle, notificationContainerStyle]}>
      <TouchableOpacity onPress={onPress}>
        <FastImage
          source={notificationIcon}
          style={{width: 24, height: 24}}
          resizeMode="contain"
        />
        {unreadNotificationCounter !== 0 && (
          <View
            style={[styles.notificationCounterStyle, notificationCounterStyle]}>
            <Text
              style={[
                styles.notificationCounterTextStyle,
                notificationCounterTextStyle,
              ]}
              numberOfLines={1}
              adjustsFontSizeToFit>
              {unreadNotificationCounter}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  notificationContainerStyle: {
    height: 50,
    justifyContent: 'center',
  },
  notificationCounterStyle: {
    position: 'absolute',
    backgroundColor: 'red',
    width: 16,
    height: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    right: 0,
    borderWidth: 2,
    borderColor: colors.white,
  },
  notificationCounterTextStyle: {
    fontSize: fontSizes.f8,
    color: colors.white,
    fontFamily: fonts.SFProTextBold,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
