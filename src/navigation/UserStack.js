import React, { useEffect } from 'react';
import {
  StyleSheet,
} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Request from '../screens/Home/screens/Request';
import Notifications from '../screens/Home/screens/Notification';
import Profile from '../screens/Home/screens/Profile';

import AddRequest from '../screens/Request/screen/AddRequest';

import SplashScreen from 'react-native-splash-screen';
import TabComponent from '../components/BottomTabComponent';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ChangePassword from '../screens/Authentication/ChangePassword';
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default function UserStack({ navigation }) {
  useEffect(() => {
    // SplashScreen.hide();
  }, []);
  const navOptions = { headerShown: false };

  return (
    <Stack.Navigator
      initialRouteName="AppStack"
      screenOptions={{
        gestureEnabled: false,
      }}
      options={{ headerLeft: () => null }}
    >
      <Stack.Screen
        name="AddRequest"
        component={AddRequest}
        options={navOptions}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={navOptions}
      />

      <Stack.Screen name="AppStack" component={AppStack} options={navOptions} />
    </Stack.Navigator>
  );
}

const AppStack = (props) => {
  return (
    <Tab.Navigator
      initialRouteName="Request"
      screenOptions={({ route }) => ({
        tabBarShowLabel: false,
        headerShown: false,
        tabBarStyle: {
          display: route.params?.drawer ? 'none' : 'flex',
          position: 'absolute',
          // bottom: 25,
          // left: 20,
          // right: 20,
          elevation: 0,
          // borderRadius: 25,
          height: 80,
          alignItems: 'center',
          justifyContent: 'center',
          borderTopWidth: 0,
          // backgroundColor: 'red',
          // ...styles.shadow,
        },
      })}
    >
      <Tab.Screen
        name="Request"
        component={Request} //DashBoard
        options={{
          tabBarButton: (props) => <TabComponent label="requests" {...props} />,
        }}
      />
      <Tab.Screen
        name="Listening"
        component={Profile} //Listening
        options={{
          tabBarButton: (props) => (
            <TabComponent label="profile" {...props} />
          ),
        }}
      />
      <Tab.Screen
        name="Reading"
        component={Notifications} //DashBoard
        options={{
          tabBarButton: (props) => <TabComponent label="notification" {...props} />,
        }}
      />
    </Tab.Navigator>
  );
};

// export default AppStack;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 1,
    shadowRadius: 25,
    elevation: 5,
    backgroundColor: 'white',
  },
});
