import React from 'react';

//Screens
import Login from '../screens/Authentication/Login';
import ChangePassword from '../screens/Authentication/ChangePassword';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();
export default function AuthStack() {
  const navOptions = { headerShown: false };

  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen name="Login" component={Login} options={navOptions} />
      {/* <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={navOptions}
      /> */}
    </Stack.Navigator>
  );
}
