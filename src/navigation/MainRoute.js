import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Platform } from 'react-native';
//Screens
import SplashAction from '../screens/SplashAction';
// Authentication Stack
import AuthStack from './AuthStack';
// Bottom Navigation Stack
import UserStack from './UserStack';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { navigationRef } from '../utils/NavigationService';
import SplashScreen from 'react-native-splash-screen';
const Stack = createNativeStackNavigator();

export default function MainStack() {
  const navOptions = { headerShown: false };

  React.useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName="splashscreen"
        screenOptions={{
          gestureEnabled: true,
        }}>
        <>
          <Stack.Screen
            name="SplashAction"
            component={SplashAction}
            options={navOptions}
          />

          {/* Authentication Stack */}
          <Stack.Screen
            name="AuthStack"
            component={AuthStack}
            options={navOptions}
          />
          {/* Bottom Navigation Stack */}
          <Stack.Screen
            name="UserStack"
            component={UserStack}
            options={navOptions}
          />
        </>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
