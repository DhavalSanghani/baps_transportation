import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducerAll from './src/store/reducers';

import MainRoute from './src/navigation/MainRoute';
import SettingProvider from './src/components/SettingProvider';
import { LogBox } from 'react-native';

LogBox.ignoreAllLogs();
// import store from './src/store';

// import configureStore from './src/store';

// const store = configureStore();
const store = createStore(reducerAll, {}, applyMiddleware(ReduxThunk));

export default function App() {
  return (
    <Provider store={store}>
      <React.Fragment>
        <SettingProvider />
        <MainRoute />
      </React.Fragment>
    </Provider>
  );
}
